return {
	-- e.g. DiffuseShaderVars, codegen'd from shaders/diffuse.*,
	-- accessible in game.cpp as shaders.diffuse
	{"Diffuse", "shaders/diffuse.vert", "shaders/diffuse.frag", "diffuse"},
	{"Text", "shaders/text.vert", "shaders/text.frag", "text"},
	--{"Particle", "shaders/particle.vert", "shaders/particle.frag", "particle"},
	{"Pass", "shaders/pass.vert", "shaders/pass.frag", "pass"},
	{"UV", "shaders/uv.vert", "shaders/uv.frag", "uv"},
	--{"Chameleon", "shaders/chameleon.vert", "shaders/chameleon.frag", "chameleon"},
	--{"Skinned", "shaders/skinned.vert", "shaders/skinned.frag", "skinned"},
	{"Envmap", "shaders/envmap.vert", "shaders/envmap.frag", "envmap"},
	{"Decal", "shaders/decal.vert", "shaders/decal.frag", "decal"},
}
