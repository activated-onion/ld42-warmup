local R = require "reflect-schema-common"

local int = R.int
local shader_uniforms = R.shader_uniforms

local defs = {}

local shaders = require "reflect-shaders"

-- Generate individual ShaderVars types
for _, shader in ipairs (shaders) do
	local typename = shader [1] .. "ShaderVars"
	
	table.insert (defs, {
		name = typename,
		type = "struct",
		fields = shader_uniforms (shader [2], shader [3]),
	})
end

return {
	includes = {},
	defs = defs,
}
