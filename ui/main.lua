local last_e = 0

local ecs = {
	rects = {},
}

local function make_entity ()
	last_e = last_e + 1
	return last_e
end

local function ordered_keys (t)
	local rc = {}
	for k, v in pairs (t) do
		table.insert (rc, k)
	end
	table.sort (rc)
	
	return rc
end

for i = 1, 5 do
	ecs.rects [make_entity ()] = {
		x = 0,
		y = 0,
		width = 100,
		height = 100,
	}
end

local dragging_e = nil

function love.mousepressed (x, y)
	for _, e in ipairs (ordered_keys (ecs.rects)) do
		local rect = ecs.rects [e]
		
		if x >= rect.x and y >= rect.y and
		   x < rect.x + rect.width and y < rect.y + rect.height 
		then
			rect.checked = not rect.checked
			dragging_e = e
		end
	end
end

function love.mousemoved (x, y, dx, dy)
	local rect = ecs.rects [dragging_e]
	
	if rect then
		rect.x = rect.x + dx
		rect.y = rect.y + dy
	end
end

function love.mousereleased (x, y)
	dragging_e = nil
end

function love.update (dt)
	
end

function love.draw ()
	local g = love.graphics
	
	for _, e in ipairs (ordered_keys (ecs.rects)) do
		local rect = ecs.rects [e]
		
		if rect.checked then
			g.setColor (1, 0, 0, 1)
		else
			g.setColor (0.5, 0.5, 0.5, 1)
		end
		g.rectangle ("fill", rect.x, rect.y, rect.width, rect.height)
	end
end
