#ifndef ACTIVATED_ONION_OFFICE_DEMO_H
#define ACTIVATED_ONION_OFFICE_DEMO_H

#include <memory>
#include <vector>

#include <glm/gtc/quaternion.hpp>

#include "AL/al.h"
#include "AL/alc.h"

#include "prns.h"

#include "mesh.h"
#include "physics.h"
#include "sdl-raii.h"
#include "shader.h"
#include "timestep.h"
#include "transform.h"

#include "audio.h"
#include "main.gen.h"
#include "shader-vars.gen.h"

struct FileSystem;

struct FramebufferRaii {
	uint32_t fbo;
	
	FramebufferRaii ();
	~FramebufferRaii ();
	
	FramebufferRaii (const FramebufferRaii & o) = delete;
	FramebufferRaii & operator = (const FramebufferRaii & o) = delete;
	
	FramebufferRaii (FramebufferRaii && o);
	FramebufferRaii & operator = (FramebufferRaii && o) = delete;
	
	uint32_t operator * () const;
	void bind () const;
	void attach_texture (uint32_t attachment, uint32_t tex_target, uint32_t tex) const;
};

struct RenderbufferRaii {
	uint32_t id;
	
	RenderbufferRaii ();
	~RenderbufferRaii ();
	
	RenderbufferRaii (const RenderbufferRaii & o) = delete;
	RenderbufferRaii & operator = (const RenderbufferRaii & o) = delete;
	
	RenderbufferRaii (RenderbufferRaii && o);
	RenderbufferRaii & operator = (RenderbufferRaii && o) = delete;
	
	uint32_t operator * () const;
	void bind () const;
};

struct Game {
	Settings settings;
	SdlRaii sdl;
	
	Textures textures;
	Meshes meshes;
	Shaders shaders;
	
	Timestep timestep;
	
	Camera camera;
	glm::vec3 camera_pos;
	glm::vec3 camera_vel;
	bool on_floor;
	
	glm::vec3 mob_pos;
	glm::vec3 light_dir;
	glm::vec3 prop_pos;
	
	PhysicsResult spray;
	MeshBuffer decal_mesh;
	size_t decal_index;
	
	bool running;
	long prev_frame_time;
	
	prns_t rng;
	
	bool paused;
	bool skipped_first_mouse;
	
	long frames;
	
	AabbWorld aabb_world;
	Aabb3 last_supporting_box;
	
	TextureRaii rtt_tex;
	RenderbufferRaii rtt_depth;
	std::vector <FramebufferRaii> rtt_fbo;
	
	Audio audio;
	
	Game (LuaLoader & loader);
	
	void do_logic (FileSystem & fs);
	Transform get_envmap_viewproj (const glm::vec3 & dir, const glm::vec3 & x, const glm::vec3 & y) const;
	Transform get_player_viewproj () const;
	void render (const Transform & viewproj, const glm::vec3 & view_pos) const;
	void render_ui () const;
	int step (FileSystem & fs);
};

#endif
