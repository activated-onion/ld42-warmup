#ifndef ACTIVATED_ONION_LD42_AUDIO_H
#define ACTIVATED_ONION_LD42_AUDIO_H

#include <functional>
#include <memory>
#include <vector>

#include "AL/al.h"
#include "AL/alc.h"

struct FileSystem;

using up_alc_device = std::unique_ptr <ALCdevice, std::function <void (ALCdevice *)> >;
using up_alc_context = std::unique_ptr <ALCcontext, std::function <void (ALCcontext *)> >;

struct Audio {
	up_alc_device dev;
	up_alc_context ctx;
	
	ALuint src_bgm;
	ALuint buffer_test;
	
	Audio ();
	~Audio ();
	
	Audio (const Audio & o) = delete;
	Audio & operator = (const Audio & o) = delete;
	
	Audio (Audio && o) = delete;
	Audio & operator = (Audio && o) = delete;
};

#endif
