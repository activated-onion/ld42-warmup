#ifndef PATHOS_RELOADABLE_SHADER_H
#define PATHOS_RELOADABLE_SHADER_H

#include <string>

#include "shader.h"
#include "shader-vars.gen.h"

struct FileSystem;

void visit_GetUniformLocation (const std::string & name, int & n, Visitor & v);

template <typename T>
void get_shader_vars (T & vars, GLuint id) {
	Visitor v;
	v.userdata = &id;
	v.visit_int = visit_GetUniformLocation;
	
	visit (vars, v);
}

template <typename T>
struct Shader {
	std::string filename;
	
	ShaderRaii vertex;
	ShaderRaii fragment;
	ShaderProgramRaii program;
	T vars;
	
	Shader () :
		vertex (GL_VERTEX_SHADER),
		fragment (GL_FRAGMENT_SHADER),
		program (),
		vars ()
	{}
	
	Shader (const Shader & o) = delete;
	Shader & operator = (const Shader & o) = delete;
	
	void reload (FileSystem & fs, const std::string & fn) {
		filename = fn;
		
		const auto vert_src = fs.load_file (filename + ".vert");
		const auto frag_src = fs.load_file (filename + ".frag");
		
		//cerr << "Shader " << filename << " " << vert_src.size () << ", " << frag_src.size () << endl;
		
		vertex.source (vert_src);
		fragment.source (frag_src);
		
		glCompileShader (*vertex);
		glCompileShader (*fragment);
		
		glAttachShader (*program, *vertex);
		glAttachShader (*program, *fragment);
		glLinkProgram (*program);
		
		get_shader_vars (vars, *program);
		
		glUseProgram (*program);
	}
};

#endif
