#ifndef PATHOS_REFLECT_MESH_H
#define PATHOS_REFLECT_MESH_H

#include <string>

#include "mesh.h"

struct Visitor;

void load (MeshBuffer & m, void * userdata);
void load (MeshBuffer & m, const std::string & name, void * userdata);

void visit (const std::string & name, MeshBuffer & m, Visitor & v);

#endif
