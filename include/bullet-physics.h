// I gave up on this!

#ifndef OFFICE_DEMO_BULLET_PHYSICS_H
#define OFFICE_DEMO_BULLET_PHYSICS_H

#include <memory>

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"

class btRigidBody;
struct btManifoldPoint;
struct btCollisionObjectWrapper;

struct BulletPhysics {
	std::unique_ptr <btCollisionConfiguration> coll_config;
	std::unique_ptr <btCollisionDispatcher> dispatcher;
	std::unique_ptr <btDbvtBroadphase> broadphase;
	std::unique_ptr <btSequentialImpulseConstraintSolver> solver;
	std::unique_ptr <btDiscreteDynamicsWorld> world;
	std::unique_ptr <btBvhTriangleMeshShape> trimesh_shape;
	std::unique_ptr <btCapsuleShapeZ> player_shape;
	std::unique_ptr <btRigidBody> player_body;
	
	std::vector <float> world_verts;
	std::vector <int32_t> world_indices;
	
	BulletPhysics (const MeshBuffer & level_mesh);
	
	void jump ();
	void apply_player_impulse (const glm::vec2 & world_motion);
	void step ();
	glm::vec3 get_player_pos () const;
	
	static bool CustomMaterialCombinerCallback(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1);
};

#endif
