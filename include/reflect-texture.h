#ifndef PATHOS_REFLECT_TEXTURE_H
#define PATHOS_REFLECT_TEXTURE_H

#include <string>

#include "texture.h"

struct Visitor;

void load (TextureRaii & m, void * userdata);
void load (TextureRaii & m, const std::string & name, void * userdata);

void visit (const std::string & name, TextureRaii & m, Visitor & v);

#endif
