#ifndef PATHOS_REFLECT_LOAD_H
#define PATHOS_REFLECT_LOAD_H

#include <string>
#include <vector>
#include "lua.hpp"

#include "file-system.h"

struct LuaLoader {
	lua_State * l;
	FileSystem & fs;
	
	LuaLoader (const std::string & filename, FileSystem & f);
	~LuaLoader ();
	
	LuaLoader (const LuaLoader & o) = delete;
	LuaLoader & operator = (const LuaLoader & o) = delete;
};

void load (std::string & s, void * userdata);
void load (int & n, void * userdata);
void load (float & f, void * userdata);
void load (bool & b, void * userdata);

void load (std::string & s, const std::string & name, void * userdata);
void load (int & n, const std::string & name, void * userdata);
void load (float & f, const std::string & name, void * userdata);
void load (bool & b, const std::string & name, void * userdata);

struct Visitor {
	void * userdata;
	void (*visit_int) (const std::string & name, int & n, Visitor & v);
	void (*visit_float) (const std::string & name, float & f, Visitor & v);
	void (*visit_string) (const std::string & name, std::string & s, Visitor & v);
	void (*visit_bool) (const std::string & name, bool & b, Visitor & v);
	
	Visitor ():
		userdata (nullptr),
		visit_int (nullptr),
		visit_float (nullptr),
		visit_string (nullptr),
		visit_bool (nullptr)
	{}
};

template <class T>
void load (std::vector <T> & v, const std::string & name, void * userdata) {
	auto l = ((LuaLoader *)userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	if (lua_type (l, -1) == LUA_TTABLE) {
		lua_len (l, -1);
		const auto len = lua_tonumber (l, -1);
		lua_pop (l, 1);
		
		v.resize (len);
		
		for (int i = 0; i < len; i++) {
			lua_pushnumber (l, i + 1);
			lua_gettable (l, -2);
			
			load (v [i], userdata);
			
			lua_pop (l, 1);
		}
	}
	
	lua_pop (l, 1);
}

template <class T>
void visit (const std::string & name, std::vector <T> & vec, Visitor & v) {
	
}

bool load_push (const std::string & name, void * userdata);
void load_pop (void * userdata);

#endif
