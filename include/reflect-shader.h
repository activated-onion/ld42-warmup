#ifndef PATHOS_REFLECT_SHADER_H
#define PATHOS_REFLECT_SHADER_H

#include <string>

#include "lua.hpp"
#include "reloadable-shader.h"

template <typename T>
struct Shader;
struct Visitor;

template <typename T>
void load (Shader <T> & sh, void * userdata) {
	std::string fn;
	
	load (fn, userdata);
	
	FileSystem & fs = ((LuaLoader *)userdata)->fs;
	
	sh.reload (fs, fn);
}

template <typename T>
void load (Shader <T> & sh, const std::string & name, void * userdata) {
	auto l = ((LuaLoader *)userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (sh, userdata);
	
	lua_pop (l, 1);
}

template <typename T>
void visit (const std::string & name, Shader <T> & sh, Visitor & v) {
	FileSystem * fs = (FileSystem *)v.userdata;
	sh.reload (*fs, sh.filename);
}

#endif
