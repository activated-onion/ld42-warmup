#ifndef LD42_WARMUP_PHYSICS_H
#define LD42_WARMUP_PHYSICS_H

#include <glm/glm.hpp>
#include <vector>

struct MeshBuffer;

struct Aabb3 {
	glm::vec3 min;
	glm::vec3 max;
	uint16_t indexes [3];
};

struct PhysicsResult {
	glm::vec3 pos;
	glm::vec3 vel;
	glm::vec3 bounce_normal;
	bool on_floor;
	Aabb3 supporting_box;
};

struct AabbWorld {
	// 6 normals
	std::vector <Aabb3> faces [6];
};

glm::vec3 get_tangent (glm::vec3 normal);
AabbWorld make_world (const MeshBuffer & mb);

PhysicsResult world_physics_2 (glm::vec3 start, glm::vec3 vel3, const AabbWorld & world, glm::vec3 player_radius, bool recurse);

std::vector <int32_t> get_decal_triangles (const AabbWorld & world, glm::vec3 normal, glm::vec3 min, glm::vec3 max);

#endif
