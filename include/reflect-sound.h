#ifndef PATHOS_REFLECT_SOUND_H
#define PATHOS_REFLECT_SOUND_H

#include <string>

#include "audio.h"

struct Visitor;

struct SoundRaii {
	ALuint s;
};

void load (SoundRaii & pb, void * userdata);
void load (SoundRaii & pb, const std::string & name, void * userdata);

void visit (const std::string & name, SoundRaii & pb, Visitor & v);

#endif
