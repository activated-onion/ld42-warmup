local Common = require "reflect-schema-common"

local str = Common.str
local int = Common.int
local float = Common.float
local map_array = Common.map_array
local shader_uniforms = Common.shader_uniforms

local shaders = require "reflect-shaders"

local defs = {
	{
		name = "Settings",
		type = "struct",
		fields = {
			str ("title", ":P"),
			int ("width", 800),
			int ("height", 480),
			int ("timestep_num", 60),
			int ("timestep_den", 1000),
			{"bool", "draw_particles", true},
			{"bool", "draw_plane", true},
			{"bool", "draw_sky", true},
			float ("mouse_speed", 1.0),
			float ("run_speed", 2.0),
			float ("gravity", 0.75),
			{"bool", "start_paused", false},
			float ("mob_scale", 1.5),
		},
	},
	
	{
		name = "Textures",
		type = "struct",
		fields = map_array ({
			{"alison", "textures/alison.png"},
			{"font", "textures/font.png"},
			{"louisa", "textures/louisa.png"},
			{"pathos", "textures/pathos.png"},
			{"shadow", "textures/shadow.png"},
			{"white", "textures/white.png"},
			{"walls", "textures/walls.png"},
		}, function (t) return {"TextureRaii", t [1], nil} end)
	},
	
	{
		name = "Meshes",
		type = "struct",
		fields = map_array ({
			{"level", "meshes/office.iqm"},
			{"bomb", "meshes/bomb.iqm"},
			{"mob", "meshes/mob.iqm"},
			{"prop", "meshes/prop.iqm"},
			{"xy_plane", "meshes/xy-plane.iqm"},
		}, function (t) return {"MeshBuffer", t [1], nil} end)
	},
	
	{
		name = "Sounds",
		type = "struct",
		fields = map_array ({
			{"bgm", "sounds/bgm.ogg"},
			{"boing", "sounds/boing.ogg"},
			{"checkpoint", "sounds/checkpoint.ogg"},
			{"chopper_gun", "sounds/chopper-gun.ogg"},
			{"explosion", "sounds/explosion.ogg"},
			{"pew", "sounds/pew.ogg"},
			{"warp", "sounds/warp.ogg"},
		}, function (t) return {"SoundRaii", t [1], nil} end)
	},
}

local shader_fields = {}

for _, shader in ipairs (shaders) do
	local typename = shader [1] .. "ShaderVars"
	
	table.insert (shader_fields, {
		"Shader <" .. typename .. ">",
		shader [4],
		nil
	})
end

table.insert (defs, {
	name = "Shaders",
	type = "struct",
	fields = shader_fields,
})

return {
	includes = {
		"reflect-mesh.h",
		"reflect-shader.h",
		"reflect-sound.h",
		"reflect-texture.h",
		"reloadable-shader.h",
		"shader-vars.gen.h",
	},
	defs = defs,
}
