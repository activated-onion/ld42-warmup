#include "SDL/SDL.h"

#include "file-system.h"
#include "game.h"
#include "stb-vorbis.h"

int main (int argc, char * argv []) {
	FileSystem fs;
	LuaLoader loader ("settings.lua", fs);
	
	Game game (loader);
	
	while (game.running) {
		const int steps = game.step (fs);
		
		if (steps == 0) {
			SDL_Delay (2);
		}
	}
	
	return 0;
}
