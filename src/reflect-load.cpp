#include "reflect-load.h"

using namespace std;

LuaLoader * cast (void * userdata) {
	return (LuaLoader *)userdata;
}

void load (string & s, void * userdata) {
	auto l = cast (userdata)->l;
	
	if (lua_type (l, -1) == LUA_TSTRING) {
		size_t len = 0;
		
		const char * c_str = lua_tolstring (l, -1, &len);
		
		s = c_str;
	}
}

void load (int & n, void * userdata) {
	auto l = cast (userdata)->l;
	
	if (lua_type (l, -1) == LUA_TNUMBER) {
		n = lua_tointeger (l, -1);
	}
}

void load (float & f, void * userdata) {
	auto l = cast (userdata)->l;
	
	if (lua_type (l, -1) == LUA_TNUMBER) {
		f = lua_tonumber (l, -1);
	}
}

void load (bool & b, void * userdata) {
	auto l = cast (userdata)->l;
	
	if (lua_type (l, -1) == LUA_TBOOLEAN) {
		b = lua_toboolean (l, -1);
	}
}

void load (string & s, const string & name, void * userdata) {
	auto l = cast (userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (s, userdata);
	
	lua_pop (l, 1);
}

void load (int & n, const string & name, void * userdata) {
	auto l = cast (userdata)->l;
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (n, userdata);
	
	lua_pop (l, 1);
}

void load (float & f, const string & name, void * userdata) {
	auto l = cast (userdata)->l;
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (f, userdata);
	
	lua_pop (l, 1);
}

void load (bool & f, const string & name, void * userdata) {
	auto l = cast (userdata)->l;
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (f, userdata);
	
	lua_pop (l, 1);
}

bool load_push (const string & name, void * userdata) {
	auto l = cast (userdata)->l;
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	return lua_type (l, -1) == LUA_TTABLE;
}

void load_pop (void * userdata) {
	auto l = cast (userdata)->l;
	lua_pop (l, 1);
}

LuaLoader::LuaLoader (const string & filename, FileSystem & f):
	fs (f)
{
	l = luaL_newstate ();
	luaL_openlibs (l);
	
	luaL_dofile (l, filename.c_str ());
}

LuaLoader::~LuaLoader () {
	lua_close (l);
}
