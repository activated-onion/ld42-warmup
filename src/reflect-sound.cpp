#include "reflect-sound.h"

#include "reflect-load.h"

#include <iostream>

using namespace std;

void load_sound (SoundRaii & pb, const FileSystem & fs, string fn) {
	vorb_to_buffer_stb (pb.s, fs, fn);
}

void load (SoundRaii & t, void * userdata) {
	string fn;
	
	load (fn, userdata);
	
	FileSystem & fs = ((LuaLoader *)userdata)->fs;
	
	load_sound (t, fs, fn);
}

void load (SoundRaii & t, const string & name, void * userdata) {
	auto l = ((LuaLoader *)userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (t, userdata);
	
	lua_pop (l, 1);
}

void visit (const string & name, SoundRaii & m, Visitor & v) {
	
}
