#include "reflect-mesh.h"
#include "reflect-load.h"

using namespace std;

void load (MeshBuffer & m, void * userdata) {
	string fn;
	
	load (fn, userdata);
	
	FileSystem & fs = ((LuaLoader *)userdata)->fs;
	
	m = MeshBuffer::load_iqm (fs.load_file (fn));
}

void load (MeshBuffer & m, const string & name, void * userdata) {
	auto l = ((LuaLoader *)userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (m, userdata);
	
	lua_pop (l, 1);
}

void visit (const string & name, MeshBuffer & m, Visitor & v) {
	
}
