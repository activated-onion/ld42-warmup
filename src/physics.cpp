#include "physics.h"

#include <algorithm>
#include <iostream>

#include "mesh.h"

using namespace glm;
using std::cerr;
using std::cout;
using std::endl;
using std::sort;
using std::vector;

vec3 get_tangent (vec3 normal) {
	if (normal.z == 0.0f) {
		return vec3 (-normal.y, normal.x, 0.0f);
	}
	else {
		return vec3 (0.0f, 1.0f, 0.0f);
	}
}

ivec3 get_tangent (ivec3 normal) {
	if (normal.z == 0) {
		return vec3 (-normal.y, normal.x, 0);
	}
	else {
		return vec3 (0, 1, 0);
	}
}

// Fucking GLM
ivec3 cross (ivec3 u, ivec3 v) {
	return ivec3 (
		u.y * v.z - u.z * v.y,
		u.z * v.x - u.x * v.z,
		u.x * v.y - u.y * v.x
	);
}

PhysicsResult apply_normal (const PhysicsResult & input, vec3 n) {
	return PhysicsResult {
		input.pos,
		input.vel,
		n,
		input.on_floor,
		input.supporting_box,
	};
}

PhysicsResult or_floor (const PhysicsResult & input, bool on_floor) {
	return PhysicsResult {
		input.pos,
		input.vel,
		input.bounce_normal,
		input.on_floor || on_floor,
		input.supporting_box,
	};
}

// True if [a, b] and [c, d] overlap
bool ranges_overlap (float a, float b, float c, float d) {
	// 1 dimensional SAT is the purest form of SAT
	
	return ! ((glm::min (a, b) > glm::max (c, d)) || (glm::min (c, d) > glm::max (a, b)));
}

bool aabbs_overlap (vec3 min1, vec3 max1, vec3 min2, vec3 max2) {
	for (int i = 0; i < 3; i++) {
		if (! ranges_overlap (min1 [i], max1 [i], min2 [i], max2 [i])) {
			return false;
		}
	}
	return true;
}

float get_radius (vec3 radii, vec3 normal) {
	return abs (dot (normal, radii));
}

int get_normal_index (vec3 normal) {
	const int num_normals = 6;
	const vec3 normals [num_normals] = {
		vec3 (-1,  0,  0),
		vec3 ( 1,  0,  0),
		vec3 ( 0, -1,  0),
		vec3 ( 0,  1,  0),
		vec3 ( 0,  0, -1),
		vec3 ( 0,  0,  1),
	};
	
	for (int i = 0; i < num_normals; i++) {
		if (normal == normals [i]) {
			return i;
		}
	}
	
	return -1;
}

vector <int32_t> get_decal_triangles (const AabbWorld & world, vec3 normal, vec3 min_0, vec3 max_0)
{
	vector <int32_t> result;
	const auto normal_index = get_normal_index (normal);
	if (normal_index < 0 || normal_index >= 6) {
		return result;
	}
	const auto & faces = world.faces [normal_index];
	
	const vec3 min = glm::min (min_0, max_0);
	const vec3 max = glm::max (min_0, max_0);
	
	for (const auto & face: faces) {
		if (aabbs_overlap (face.min, face.max, min, max)) {
			for (int i = 0; i < 3; i++) {
				result.push_back (face.indexes [i]);
			}
		}
	}
	
	return result;
}

PhysicsResult world_physics_2 (vec3 start, vec3 vel3, const AabbWorld & world, vec3 player_radii, bool recurse) 
{
	const int num_normals = 6;
	const vec3 normals [num_normals] = {
		vec3 (-1,  0,  0),
		vec3 ( 1,  0,  0),
		vec3 ( 0, -1,  0),
		vec3 ( 0,  1,  0),
		vec3 ( 0,  0, -1),
		vec3 ( 0,  0,  1),
	};
	
	int first_normal = -1;
	float first_t = 0.0f;
	bool on_floor = false;
	Aabb3 supporting_box;
	
	for (int i = 0; i < num_normals; i++) {
		const vec3 & normal = normals [i];
		const float foo = -dot (normal, vel3);
		
		// This eliminates 3-5 of the normals since all faces are one-sided
		if (foo > 0.0f) {
			const vec3 tangent = get_tangent (normal);
			const vec3 bitangent = cross (normal, tangent);
			
			const float player_radius = get_radius (player_radii, normal);
			const float tangent_radius = get_radius (player_radii, tangent);
			const float bt_radius = get_radius (player_radii, bitangent);
			
			const auto start_n = dot (normal, start) - player_radius;
			const auto stop_n = start_n + dot (normal, vel3);
			
			const auto & faces = world.faces [i];
			for (const auto & face: faces) {
				const auto face_n = dot (normal, face.min);
				
				if (ranges_overlap (start_n, stop_n, face_n, face_n)) {
					const float t = (face_n - start_n) / (stop_n - start_n);
					//cerr << t << endl;
					const auto dest = start + t * vel3;
					
					const auto start_t = dot (tangent, dest) - tangent_radius;
					const auto stop_t = start_t + 2 * tangent_radius;
					const auto face_a_t = dot (tangent, face.min);
					const auto face_b_t = dot (tangent, face.max);
					
					if (ranges_overlap (start_t, stop_t, face_a_t, face_b_t)) {
						const auto start_bt = dot (bitangent, dest) - bt_radius;
						const auto stop_bt = start_bt + 2 * bt_radius;
						const auto face_a_bt = dot (bitangent, face.min);
						const auto face_b_bt = dot (bitangent, face.max);
						
						if (ranges_overlap (start_bt, stop_bt, face_a_bt, face_b_bt)) {
							if (first_normal == -1 || t < first_t) {
								first_normal = i;
								first_t = t;
								
								if (i == 5) {
									on_floor = true;
									supporting_box = face;
								}
							}
						}
					}
				}
			}
		}
	}
	
	if (first_normal != -1) {
		const float epsilon = 1.f / 128.f;
		const auto & normal = normals [first_normal];
		const vec3 fixed_dir = vel3 - normal * dot (vel3, normal);
		const auto dest = start + first_t * vel3 + epsilon * normal;
		
		//cerr << supporting_box.min.x << " " << supporting_box.min.y << " " << supporting_box.max.x << " " << supporting_box.max.y << endl;
		
		if (recurse) {
			auto result = or_floor (apply_normal (world_physics_2 (dest, fixed_dir, world, player_radii, recurse), normal), on_floor);
			result.supporting_box = supporting_box;
			return result;
		}
		else {
			return PhysicsResult {
				dest,
				fixed_dir,
				normal,
				on_floor,
			};
		}
	}
	else {
		return PhysicsResult {
			start + vel3,
			vel3,
			vec3 (0.0f),
			on_floor,
		};
	}
}

AabbWorld make_world (const MeshBuffer & mb) {
	AabbWorld rc;
	
	const vec3 normals [] = {
		vec3 (-1,  0,  0),
		vec3 ( 1,  0,  0),
		vec3 ( 0, -1,  0),
		vec3 ( 0,  1,  0),
		vec3 ( 0,  0, -1),
		vec3 ( 0,  0,  1),
	};
	
	for (uint32_t i = 0; i < mb.indexes.size (); i += 3) {
		const vec3 normal = mb.verts [mb.indexes [i + 0]].normal;
		
		int j = -1;
		for (int k = 0; k < 6; k++) {
			if (normal == normals [k]) {
				j = k;
				break;
			}
		}
		
		if (j == -1) {
			continue;
		}
		
		const vec3 verts [3] = {
			vec3 (mb.verts [mb.indexes [i + 0]].pos),
			vec3 (mb.verts [mb.indexes [i + 1]].pos),
			vec3 (mb.verts [mb.indexes [i + 2]].pos),
		};
		
		const Aabb3 box {
			min (min (verts [0], verts [1]), verts [2]),
			max (max (verts [0], verts [1]), verts [2]),
			{
				mb.indexes [i + 0],
				mb.indexes [i + 1],
				mb.indexes [i + 2],
			},
		};
		
		rc.faces [j].push_back (box);
	}
	
	for (int j = 0; j < 6; j++) {
		const vec3 normal = normals [j];
		
		sort (rc.faces [j].begin (), rc.faces [j].end (), [normal] (const Aabb3 & a, const Aabb3 & b) -> bool {
			return dot (a.min, normal) > dot (b.min, normal);
		});
	}
	
	return rc;
}
