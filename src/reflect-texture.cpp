#include "reflect-texture.h"

#include "image.h"
#include "reflect-load.h"

using namespace std;

void load_texture (int id, const Image & img) {
	glBindTexture (GL_TEXTURE_2D, id);
	
	auto format = GL_RGB;
	switch (img.channels) {
		case 4:
			format = GL_RGBA;
			break;
	}
	
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D (GL_TEXTURE_2D, 0, format, img.w, img.h, 0, format, GL_UNSIGNED_BYTE, &img.pixels [0]);
	glGenerateMipmap (GL_TEXTURE_2D);
}

void load_texture (int id, const FileSystem & fs, string fn) {
	const auto compressed = fs.load_file (fn);
	load_texture (id, Image (compressed));
}

void load (TextureRaii & t, void * userdata) {
	string fn;
	
	load (fn, userdata);
	
	FileSystem & fs = ((LuaLoader *)userdata)->fs;
	
	load_texture (*t, fs, fn);
}

void load (TextureRaii & t, const string & name, void * userdata) {
	auto l = ((LuaLoader *)userdata)->l;
	
	lua_pushlstring (l, name.c_str (), name.size ());
	lua_gettable (l, -2);
	
	load (t, userdata);
	
	lua_pop (l, 1);
}

void visit (const string & name, TextureRaii & m, Visitor & v) {
	
}
