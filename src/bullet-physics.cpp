#include "bullet-physics.h"

BulletPhysics::BulletPhysics (const MeshBuffer & level_mesh) {
	coll_config = make_unique <btDefaultCollisionConfiguration> ();
	dispatcher = make_unique <btCollisionDispatcher> (&*coll_config);
	broadphase = make_unique <btDbvtBroadphase> ();
	solver = make_unique <btSequentialImpulseConstraintSolver> ();
	world = make_unique <btDiscreteDynamicsWorld> (&*dispatcher, &*broadphase, &*solver, &*coll_config);
	
	world->setGravity (btVector3 (0, 0, -9.8));
	world->getDispatchInfo ().m_useContinuous = true;
	
	world_verts.resize (3 * level_mesh.verts.size ());
	world_indices.resize (level_mesh.indexes.size ());
	
	for (size_t i = 0; i < level_mesh.verts.size (); i++) {
		const auto & vert = level_mesh.verts [i];
		world_verts [i * 3 + 0] = vert.pos [0];
		world_verts [i * 3 + 1] = vert.pos [1];
		world_verts [i * 3 + 2] = vert.pos [2];
	}
	
	for (size_t i = 0; i < level_mesh.indexes.size (); i++) {
		world_indices [i] = level_mesh.indexes [i];
	}
	
	{
		// Possible leak
		btTriangleIndexVertexArray * triangle_array = new btTriangleIndexVertexArray (world_indices.size () / 3, world_indices.data (), sizeof (int) * 3, world_verts.size () / 3, world_verts.data (), sizeof (float) * 3);
		
		// Possible leak
		trimesh_shape = make_unique <btBvhTriangleMeshShape> (triangle_array, true);
		
		btTriangleInfoMap* triangle_info_map = new btTriangleInfoMap();
		btGenerateInternalEdgeInfo (&*trimesh_shape, triangle_info_map);
		
		btTransform ground_transform;
		ground_transform.setIdentity ();
		
		btScalar mass (0);
		btVector3 local_inertia (0, 0, 0);
		
		// Possible leaks abound
		btDefaultMotionState * motion_state = new btDefaultMotionState (ground_transform);
		btRigidBody::btRigidBodyConstructionInfo info (mass, motion_state, &*trimesh_shape, local_inertia);
		btRigidBody * body = new btRigidBody (info);
		body->setFriction (0.25f);
		
		body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK | btCollisionObject::CF_KINEMATIC_OBJECT);
		
		world->addRigidBody (body);
	}
	
	{
		btBoxShape * box_shape = new btBoxShape (btVector3 (0.5 + 0.0625, 0.5 + 0.0625, 1));
		
		btScalar mass (0);
		btVector3 local_inertia (0, 0, 0);
		
		for (int y = -16; y < 16; y++) {
			for (int x = -24; x < 24; x++) {
				btTransform transform;
				transform.setIdentity ();
				transform.setOrigin (btVector3 (x, y, -0.9f));
				
				btDefaultMotionState * motion_state = new btDefaultMotionState (transform);
				btRigidBody::btRigidBodyConstructionInfo info (mass, motion_state, box_shape, local_inertia);
				btRigidBody * body = new btRigidBody (info);
				body->setFriction (0.25f);
				
				body->setCollisionFlags(body->getCollisionFlags() |btCollisionObject::CF_KINEMATIC_OBJECT);
				
				world->addRigidBody (body);
			}
		}
	}
	
	{
		float player_radius = 0.25f;
		float player_height = 1.7f - 0.1f;
		
		player_shape = make_unique <btCapsuleShapeZ> (player_radius, player_height - 2.0f * player_radius);
		
		btScalar mass (1.0f);
		
		btVector3 local_inertia (0.0f, 0.0f, 0.0f);
		player_shape->calculateLocalInertia (mass, local_inertia);
		
		btTransform player_spawn;
		player_spawn.setIdentity ();
		player_spawn.setOrigin (btVector3 (0.f, 0.f, 5.f));
		
		btDefaultMotionState * motion_state = new btDefaultMotionState (player_spawn);
		btRigidBody::btRigidBodyConstructionInfo info (mass, motion_state, &*player_shape, local_inertia);
		
		player_body = make_unique <btRigidBody> (info);
		
		player_body->setAngularFactor (0.0);
		player_body->setFriction (0.0);
		player_body->setActivationState(DISABLE_DEACTIVATION);
		
		player_body->setCollisionFlags(player_body->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
		
		world->addRigidBody (&*player_body);
	}
}

void BulletPhysics::jump () {
	player_body->applyCentralImpulse (btVector3 (0, 0, 8));
}

void BulletPhysics::apply_player_impulse (const vec2 & world_motion) {
	const btVector3 desired_velocity (world_motion.x, world_motion.y, 0.0f);
	btVector3 diff = desired_velocity - player_body->getLinearVelocity ();
	diff.setZ (0.0f);
	
	const btVector3 impulse = diff * 0.25f;
	
	player_body->applyCentralImpulse (impulse);
}

void BulletPhysics::step () {
	world->stepSimulation (1.0 / 60.0);
}

vec3 BulletPhysics::get_player_pos () const {
	const btMotionState * ms = player_body->getMotionState ();
	btTransform transform;
	ms->getWorldTransform (transform);
	const float player_height = 1.7f;
	
	const auto v = transform * btVector3 (0.0f, 0.0f, player_height * 0.5f);
	
	return vec3 (v.x (), v.y (), v.z ());
}

// Shrug https://hub.jmonkeyengine.org/t/physics-shapes-collide-on-flat-surfaces-triangle-boundaries/35946
bool BulletPhysics::CustomMaterialCombinerCallback(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1) {
    // Find the triangle mesh object and the other object
    const btCollisionObjectWrapper *trimesh = colObj0Wrap, *other = colObj1Wrap;
    int trimesh_pid = partId0, other_pid = partId1;
    int trimesh_id = index0, other_id = index1;
    // Make sure we're giving btAdjustInternalEdgeContacts the right arguments 
    if ( colObj1Wrap->getCollisionShape()->getShapeType() == TRIANGLE_SHAPE_PROXYTYPE ) {
        trimesh = colObj1Wrap;
        trimesh_pid = partId1;
        trimesh_id = index1;

        other = colObj0Wrap;
        other_pid = partId0;
        other_id = index0;
    }

    btAdjustInternalEdgeContacts(cp, trimesh, other, trimesh_pid, trimesh_id);

    float friction0 = other->getCollisionObject()->getFriction();
    float friction1 = trimesh->getCollisionObject()->getFriction();
    float restitution0 = other->getCollisionObject()->getRestitution();
    float restitution1 = trimesh->getCollisionObject()->getRestitution();

    if (other->getCollisionObject()->getCollisionFlags() & btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK) {
        friction0 = 1.0; //partId0,index0
        restitution0 = 0.f;
    }
    if (trimesh->getCollisionObject()->getCollisionFlags() & btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK) {
        if (trimesh_pid & 1) {
            friction1 = 1.0f; //partId1,index1
        } else {
            friction1 = 0.f;
        }
        restitution1 = 0.f;
    }

    cp.m_combinedFriction = friction0 * friction1;
    cp.m_combinedRestitution = restitution0 * restitution1;
    
    return true;
}

extern ContactAddedCallback gContactAddedCallback = BulletPhysics::CustomMaterialCombinerCallback;
