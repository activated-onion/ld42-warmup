#define LD41_REFLECTED_IMPL

#include "game.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include "glm/gtc/matrix_transform.hpp"
#include "SDL2/SDL.h"
#include "AL/alext.h"

#include "file-system.h"
#include "input.h"
#include "reloadable-shader.h"
#include "text.h"

using namespace glm;
using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::make_unique;
using std::ostream;
using std::ofstream;
using std::string;
using std::stringstream;
using std::unique_ptr;
using std::vector;

template <typename T>
void set_pointers (const T & vars, const MeshVertex * first_vert) {
	const auto stride = sizeof (MeshVertex);
	
	glVertexAttribPointer (vars.attr_pos, 3, GL_FLOAT, false, stride, &first_vert->pos);
	glVertexAttribPointer (vars.attr_uv, 2, GL_FLOAT, false, stride, &first_vert->uv);
	glVertexAttribPointer (vars.attr_normal, 3, GL_FLOAT, false, stride, &first_vert->normal);
}
/*
template <typename T>
void set_pointers (const T & vars, const ParticleVertex * first_vert) {
	const auto stride = sizeof (MeshVertex);
	
	glVertexAttribPointer (vars.attr_pos, 3, GL_FLOAT, false, stride, &first_vert->pos);
	glVertexAttribPointer (vars.attr_uv, 2, GL_FLOAT, false, stride, &first_vert->uv);
	glVertexAttribPointer (vars.attr_color, 4, GL_FLOAT, false, stride, &first_vert->color);
}
*/
void reload (Shaders & shaders, FileSystem & fs) {
	Visitor v;
	v.userdata = &fs;
	
	visit (shaders, v);
}

void dump_mesh_as_obj (const MeshBuffer & mb, ostream & o) {
	o << string ("s off") << endl;
	o << endl;
	
	for (const auto & vert: mb.verts) {
		const auto pos = vert.pos;
		o << "v " << pos.x << " " << pos.y << " " << pos.z << endl;
	}
}

FramebufferRaii::FramebufferRaii () {
	glGenFramebuffers (1, &fbo);
}

FramebufferRaii::FramebufferRaii (FramebufferRaii && o):
	fbo (0)
{
	fbo = o.fbo;
	o.fbo = 0;
}

FramebufferRaii::~FramebufferRaii () {
	if (fbo != 0) {
		glDeleteFramebuffers (1, &fbo);
	}
}

uint32_t FramebufferRaii::operator * () const {
	return fbo;
}

void FramebufferRaii::bind () const {
	glBindFramebuffer (GL_FRAMEBUFFER, fbo);
}

void FramebufferRaii::attach_texture (uint32_t attachment, uint32_t tex_target, uint32_t tex) const
{
	const uint32_t level = 0;
	glFramebufferTexture2D (GL_FRAMEBUFFER, attachment, tex_target, tex, level);
}

RenderbufferRaii::RenderbufferRaii () {
	glGenRenderbuffers (1, &id);
}

RenderbufferRaii::RenderbufferRaii (RenderbufferRaii && o):
	id (0)
{
	id = o.id;
	o.id = 0;
}

RenderbufferRaii::~RenderbufferRaii () {
	if (id != 0) {
		glDeleteRenderbuffers (1, &id);
	}
}

uint32_t RenderbufferRaii::operator * () const {
	return id;
}

void RenderbufferRaii::bind () const {
	glBindRenderbuffer (GL_RENDERBUFFER, id);
}

Game::Game (LuaLoader & loader) :
	settings ("settings", &loader),
	sdl (settings.width, settings.height),
	textures ("textures", &loader),
	meshes ("meshes", &loader),
	shaders ("shaders", &loader),
	timestep (settings.timestep_num, settings.timestep_den),
	camera (),
	camera_pos (0.0f),
	camera_vel (0.0f),
	on_floor (false),
	mob_pos (camera_pos),
	light_dir (0.0f, 0.0f, 1.0f),
	spray (),
	decal_mesh (),
	decal_index (0),
	prop_pos (vec3 (60, 71, 0) + vec3 (0.5f, 0.5f, 0.0f)),
	running (true),
	prev_frame_time (0),
	rng {0},
	paused (settings.start_paused),
	skipped_first_mouse (false),
	frames (0),
	aabb_world (),
	last_supporting_box (),
	rtt_tex (),
	rtt_depth (),
	rtt_fbo ()
{
	SDL_SetWindowTitle (&*sdl.sdl, settings.title.c_str ());
	//SDL_WM_SetCaption (settings.title.c_str (), nullptr);
	//glFrontFace (GL_CW);
	glEnable (GL_TEXTURE_2D);
	glEnable (GL_TEXTURE_CUBE_MAP);
	glEnable (GL_DEPTH_TEST);
	
	camera.yaw = 180.0f;
	camera.pitch = 0.0f;
	
	camera_pos = vec3 (54 + 0.5f, 73 + 0.5f, 1.0f);
	camera_vel = vec3 (0.0f);
	on_floor = false;
	
	//mob_pos = vec3 (61.5f, 70.5f, 0.0f);
	mob_pos = vec3 (65.5f, 73.5f, 0.0f);
	
	prev_frame_time = SDL_GetTicks ();
	
	SDL_SetRelativeMouseMode(! paused ? SDL_TRUE : SDL_FALSE);
	SDL_ShowCursor (false);
	
	aabb_world = make_world (meshes.level);
	light_dir = normalize (vec3 (1.0f, 2.0f, 4.0f));
	
	if (false) {
		//const auto rc = world_physics_2 (
		//cerr << aabb_world.faces [5].size () << endl;
		
		ofstream f ("debug.obj", ofstream::binary);
		dump_mesh_as_obj (meshes.level, f);
	}
	
	{
		vector <uint8_t> tex_data;
		tex_data.resize (256 * 256 * 4);
		
		const uint32_t targets [6] = {
			GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
			GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		};
		
		rtt_fbo.resize (6);
		rtt_depth.bind ();
		
		for (int i = 0; i < 6; i++) {
			const auto target = targets [i];
			const auto tex = *rtt_tex;
			
			//cerr << target << endl;
			
			glBindTexture (GL_TEXTURE_CUBE_MAP, tex);
			
			for (uint32_t j = 0; j < tex_data.size (); j += 4) {
				tex_data [j + 0] = 0;
				tex_data [j + 1] = 255 * i / 6;
				tex_data [j + 2] = 0;
				tex_data [j + 3] = 255;
			}
			
			glTexImage2D (target, 0, GL_RGBA, 256, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_data.data ());
			
			rtt_fbo [i].bind ();
			rtt_fbo [i].attach_texture (GL_COLOR_ATTACHMENT0, target, tex);
			
			glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, 256, 256);
			glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, *rtt_depth);
			
			//cerr << (int)(glCheckFramebufferStatus (GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) << endl;
		}
		
		glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
	
	textures.pathos.bind ();
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	decal_mesh.verts.resize (340 * 3);
	decal_mesh.indexes.resize (decal_mesh.verts.size ());
}

Transform Game::get_envmap_viewproj (const vec3 & dir, const vec3 & x, const vec3 & y) const {
	const float near_p = 0.0625f;
	const mat4 projection = frustum (
		-near_p,
		near_p,
		-near_p,
		near_p,
		near_p,
		128.0f
	);
	
	const auto view_mat = Transform ()
		.translate (-prop_pos)
		.translate (-vec3 (0.0f, 0.0f, 1.7f))
		//.scale (vec3 (-1, -1, -1))
		.basis (x, y, dir)
	;
	
	return Transform ()
		.apply (view_mat.m)
		.apply (projection)
	;
}

Transform get_view_mat (const Camera & camera, vec3 camera_pos) {
	return Transform ()
		.translate (-camera_pos)
		.translate (-vec3 (0.0f, 0.0f, 0.75f))
		.scale (vec3 (1, -1, 1))
		.rotate (radians (camera.yaw), vec3 (0, 0, 1))
		.rotate (radians (camera.pitch - 90.0f), vec3 (1, 0, 0))
	;
}

Transform Game::get_player_viewproj () const {
	const float aspect = (float)settings.width / (float)settings.height;
	const float near_p = 0.0625f;
	const float fov = 0.5f;
	const mat4 projection = frustum (
		-aspect * fov * near_p,
		aspect * fov * near_p,
		-fov * near_p,
		fov * near_p,
		near_p,
		128.0f
	);
	
	const auto view_mat = get_view_mat (camera, camera_pos);
	
	return Transform ()
		.apply (view_mat.m)
		.apply (projection)
	;
}

void Game::do_logic (FileSystem & fs) {
	const auto btns = get_kbd_buttons ();
	
	bool step = false;
	
	SDL_Event event;
	while (SDL_PollEvent (&event)) {
		switch (event.type) {
			case SDL_QUIT:
				running = false;
				return;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						running = false;
						break;
					case SDLK_p:
						paused = ! paused;
						SDL_SetRelativeMouseMode(! paused ? SDL_TRUE : SDL_FALSE);
						break;
					case SDLK_i:
						step = true;
						break;
					case SDLK_SPACE:
						if (on_floor) {
							camera_vel.z = 20.0f / 60.f;
						}
						break;
					case SDLK_RCTRL:
						camera.yaw = 45.0f * (int)((camera.yaw + 360.0f + 22.5f) / 45.0f);
						break;
					case SDLK_m:
						prop_pos = camera_pos + vec3 (0.0f, 0.0f, -0.9f);
						break;
					default:
						break;
				}
				break;
			case SDL_APP_DIDENTERFOREGROUND:
				reload (shaders, fs);
				break;
			case SDL_MOUSEMOTION:
				if (skipped_first_mouse && ! paused) {
					const auto motion = event.motion;
					const float speed = 0.25f * settings.mouse_speed;
					
					camera.yaw += motion.xrel * speed;
					camera.pitch += motion.yrel * speed;
				}
				skipped_first_mouse = true;
				break;
			case SDL_MOUSEBUTTONDOWN:
			{
				const auto view_mat = get_view_mat (camera, camera_pos);
				const vec3 aim_dir = vec3 (inverse (view_mat.m) * vec4 (0.0f, 0.0f, -1.0f, 0.0f));
				
				if (event.button.button == 1) {
					const auto cast_result = world_physics_2 (camera_pos + vec3 (0.0f, 0.0f, 0.75f), aim_dir * 64.0f, aabb_world, vec3 (0.0f), false);
					
					spray = cast_result;
					// Account for Chastity's epsilon
					spray.pos = cast_result.pos + (-1.0f / 128.0f) * spray.bounce_normal;
					
					const vec3 p (camera_pos.x, camera_pos.y, 0.0f);
					const auto normal = spray.bounce_normal;
					const auto tangent = get_tangent (normal);
					const auto bitangent = cross (normal, tangent);
					
					const auto random_spin = (prns_next (&rng) & 0xff) * 3.1415926f * 2.0f / 256.0f;
					
					const auto decal_mat = inverse (Transform ()
						.translate (vec3 (-0.5f, -0.5f, 0.0f))
						.scale (vec3 (-2.0f, -2.0f, 1.0f))
						.rotate (random_spin, vec3 (0.0f, 0.0f, 1.0f))
						.basis (tangent, bitangent, normal)
						.translate (spray.pos)
					.m);
					
					const auto decal_tris = get_decal_triangles (aabb_world, normal, spray.pos - tangent - bitangent - normal * 0.25f, spray.pos + tangent + bitangent + normal * 0.25f);
					
					//decal_mesh.verts.resize (decal_tris.size ());
					//decal_mesh.indexes.resize (decal_tris.size ());
					
					auto & verts = decal_mesh.verts;
					auto & indices = decal_mesh.indexes;
					
					for (uint16_t i = 0; i < decal_tris.size (); i++) {
						const auto j = i + decal_index;
						
						verts [j] = meshes.level.verts [decal_tris [i]];
						verts [j].uv = vec3 (decal_mat * vec4 (vec3 (verts [j].pos), 1.0f));
						
						indices [j] = j;
					}
					
					decal_index = (decal_index + decal_tris.size ()) % decal_mesh.indexes.size ();
				}
				else if (event.button.button == 3) {
					light_dir = -aim_dir;
				}
			}
				break;
			default:
				break;
		}
	}
	
	if (paused && ! step) {
		return;
	}
	
	vec3 motion (0.0f);
	
	if (btns.get (EButton::Left)) {
		motion.x -= 1.0f;
	}
	if (btns.get (EButton::Right)) {
		motion.x += 1.0f;
	}
	if (btns.get (EButton::Up)) {
		motion.y += 1.0f;
	}
	if (btns.get (EButton::Down)) {
		motion.y -= 1.0f;
	}
	
	if (dot (motion, motion) > 1.0f) {
		motion = normalize (motion);
	}
	
	const vec2 forward2 (sin (glm::radians (camera.yaw)), -cos (glm::radians (camera.yaw)));
	const vec2 right (-forward2.y, forward2.x);
	
	const float speed = settings.run_speed / 60.f;
	const vec2 world_motion = speed * (forward2 * motion.y + right * motion.x);
	// Gravity
	camera_vel.z -= settings.gravity / 60.f;
	
	const vec3 player_radii (
		0.25f,
		0.25f,
		0.9f
	);
	const vec3 wall_radii (
		0.0625f,
		0.0625f,
		0.0625f
	);
	
	const auto result = world_physics_2 (camera_pos, vec3 (world_motion.x, world_motion.y, camera_vel.z), aabb_world, player_radii, true);
	last_supporting_box = result.supporting_box;
	
	if (isnan (result.pos.x)) {
		cerr << "Caught NaN" << endl;
		cerr << world_motion.x << ", " << world_motion.y << ", " << camera_vel.z << endl;
	}
	else {
		on_floor = result.on_floor;
		camera_pos = result.pos;
		camera_vel.z = result.vel.z;
	}
	
	frames++;
}

struct LightContext {
	vec3 light_dir;
	vec3 sky_dir;
};

template <typename T>
void apply_lights (const T & shader, const LightContext & lights, const mat4 & model_mat) 
{
	const auto & light_dir = lights.light_dir;
	const auto & sky_dir = lights.sky_dir;
	const auto light_mat = inverse (model_mat);
	const vec3 local_light_dir = vec3 (light_mat * vec4 (light_dir, 0.0f));
	const vec3 local_sky_dir = vec3 (light_mat * vec4 (sky_dir, 0.0f));
	glUniform3fv (shader.vars.uni_light_dir, 1, &local_light_dir [0]);
	glUniform3fv (shader.vars.uni_sky_dir, 1, &local_sky_dir [0]);
}



void Game::render (const Transform & viewproj, const vec3 & view_pos) const {
	if (true) {
		glClearColor (89.f / 255.f, 198.f / 255.f, 228.f / 255.f, 1.0f);
		glClear (GL_COLOR_BUFFER_BIT);
	}
	
	glEnable (GL_CULL_FACE);
	glDepthMask (GL_TRUE);
	glDisable (GL_BLEND);
	
	glClear (GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	const vec4 sky_dir (0.0f, 0.0f, 1.0f, 0.0f);
	
	const LightContext light_ctx {
		light_dir,
		sky_dir,
	};
	
	{
		const auto & shader = shaders.pass;
		const auto & mesh = meshes.level;
		
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		
		textures.walls.bind ();
		
		const auto model_mat = Transform ()
			.translate (vec3 (0.0f))
		.m;
		
		const auto mvp = Transform ()
			.apply (model_mat)
			.apply (viewproj.m)
		.m;
		
		apply_lights (shader, light_ctx, model_mat);
		
		glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
		glUniform1f (shader.vars.uni_depth_adjust, 0.0f / 64.f);
		glUniform4f (shader.vars.uni_tint, 1.0f, 1.0f, 1.0f, 1.0f);
		
		glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
	}
	
	//glDepthFunc (GL_EQUAL);
	//glEnable (GL_BLEND);
	if (false) {
		const auto & shader = shaders.pass;
		const auto & mesh = meshes.level;
		
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		
		textures.white.bind ();
		
		const auto model_mat = Transform ()
			.translate (vec3 (0.0f))
		.m;
		
		const auto mvp = Transform ()
			.apply (model_mat)
			.apply (viewproj.m)
		.m;
		
		apply_lights (shader, light_ctx, model_mat);
		
		glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
		glUniform1f (shader.vars.uni_depth_adjust, 0.0f / 64.f);
		glUniform4f (shader.vars.uni_tint, 1.0f, 0.0f, 1.0f, 0.125f);
		
		//glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), decal_indices.size (), GL_UNSIGNED_SHORT, decal_indices.data ());
	}
	
	glDepthFunc (GL_LESS);
	glDisable (GL_BLEND);
	if (true) {
		const auto & shader = shaders.envmap;
		const auto & mesh = meshes.prop;
		
		//glDisable (GL_BLEND);
		
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		
		glBindTexture (GL_TEXTURE_CUBE_MAP, *rtt_tex);
		
		const vec3 pos = prop_pos + vec3 (0.0f, 0.0f, 1.7f);
		
		const vec3 view_dir = pos - (camera_pos + vec3 (0.0f, 0.0f, 0.75f));
		glUniform3fv (shader.vars.uni_view_dir, 1, &view_dir [0]);
		
		const auto model_mat = Transform ()
			.scale (0.5f)
			.translate (pos)
		.m;
		
		const auto mvp = Transform ()
			.apply (model_mat)
			.apply (viewproj.m)
		.m;
		
		apply_lights (shader, light_ctx, model_mat);
		
		glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
		glUniform4f (shader.vars.uni_tint, 1.0f, 1.0f, 1.0f, 1.0f);
		
		glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
	}
	
	if (true) {
		const auto & shader = shaders.pass;
		
		const auto & mesh = meshes.bomb;
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		glBindTexture (GL_TEXTURE_2D, *textures.white);
		
		vec3 points [0] = {
			//cast_result.pos,
			//last_supporting_box.min,
			//last_supporting_box.max,
		};
		
		for (int i = 0; i < 1; i++) {
			const auto model_mat = Transform ()
				.scale (0.125f)
				.rotate (frames % 60, vec3 (1.0f, 0.0f, 0.0f))
				.translate (points [i])
			.m;
			
			const auto mvp = Transform ()
				.apply (model_mat)
				.apply (viewproj.m)
			.m;
			
			glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
			
			glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
		}
	}
	
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask (GL_FALSE);
	
	if (true) {
		const auto & shader = shaders.pass;
		
		const auto & mesh = meshes.xy_plane;
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		glBindTexture (GL_TEXTURE_2D, *textures.shadow);
		
		apply_lights (shader, light_ctx, mat4 (1.0f));
		
		const vec3 shadows [2] = {
			mob_pos,
			prop_pos,
		};
		
		glUniform1f (shader.vars.uni_depth_adjust, -1.0f / 256.f);
		glUniform4f (shader.vars.uni_tint, 0.0f, 0.0f, 0.1f, 0.75f);
		
		for (int i = 0; i < 2; i++) {
			const auto model_mat = Transform ()
				.scale (settings.mob_scale * 0.5f)
				.translate (shadows [i])
			.m;
			
			const auto mvp = Transform ()
				.apply (model_mat)
				.apply (viewproj.m)
			.m;
			
			glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
			
			glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
		}
	}
	
	glDepthFunc (GL_EQUAL);
	{
		const auto & shader = shaders.pass;
		
		const auto & mesh = decal_mesh;
		glUseProgram (*shader.program);
		
		set_pointers (shader.vars, mesh.verts.data ());
		glBindTexture (GL_TEXTURE_2D, *textures.pathos);
		
		const auto model_mat = Transform ()
			.translate (vec3 (0.0f))
		.m;
		
		const auto mvp = Transform ()
			.apply (model_mat)
			.apply (viewproj.m)
		.m;
		
		apply_lights (shader, light_ctx, model_mat);
		
		glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
		glUniform1f (shader.vars.uni_depth_adjust, 0.0f / 64.f);
		glUniform4f (shader.vars.uni_tint, 1.0f, 1.0f, 1.0f, 1.0f);
		
		glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
	}
	glDepthFunc (GL_LESS);
	
	//glDisable (GL_CULL_FACE);
	if (true) {
		const auto & shader = shaders.pass;
		
		const auto & mesh = meshes.mob;
		glUseProgram (*shader.program);
		set_pointers (shader.vars, mesh.verts.data ());
		
		glUniform1f (shader.vars.uni_depth_adjust, -1.0f / 64.f);
		glUniform4f (shader.vars.uni_tint, 1.0f, 1.0f, 1.0f, 1.0f);
		glUniform1f (shader.vars.uni_fullbright, 1.0f);
		
		const uint32_t sprites [] = {
			*textures.louisa,
			*textures.alison,
		};
		
		const auto view_mat = get_view_mat (camera, camera_pos);
		const vec3 aim_dir = vec3 (inverse (view_mat.m) * vec4 (0.0f, 0.0f, -1.0f, 0.0f));
		
		const vec3 look_vecs [] = {
			normalize (vec3 (vec2 (view_pos - mob_pos), 0.0f)),
			normalize (vec3 (vec2 (aim_dir), 0.0f)),
		};
		
		const float scales [] = {
			settings.mob_scale,
			1.7f,
		};
		
		const vec3 positions [] = {
			mob_pos,
			camera_pos + vec3 (0.0f, 0.0f, -0.9f),
		};
		
		for (int i = 0; i < 2; i++) {
			glBindTexture (GL_TEXTURE_2D, sprites [i]);
			
			const auto model_mat = Transform ()
				.translate (vec3 (0.0f, 0.075f, 0.0f))
				.point_at (look_vecs [i], vec3 (0, 0, 1))
				.scale (scales [i])
				.translate (positions [i] + vec3 (0.0f, 0.0f, -0.1f))
			.m;
			
			const auto mvp = Transform ()
				.apply (model_mat)
				.apply (viewproj.m)
			.m;
			
			glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
			
			glDrawRangeElements (GL_TRIANGLES, 0, mesh.verts.size (), mesh.indexes.size (), GL_UNSIGNED_SHORT, mesh.indexes.data ());
		}
		
		glUniform1f (shader.vars.uni_fullbright, 0.0f);
	}
}

void Game::render_ui () const {
	stringstream ss;
	ss << ":V";
	
	const MeshBuffer line = layout_text_mesh (ss.str ());
	
	const auto mvp = Transform ()
		//.translate (vec3 (-200.f, -120.f, 0.f))
		.translate (vec3 (10.f, 10.f, 0.f))
		.scale (vec3 (4.f / settings.width, -4.f / settings.height, 1.f))
		.translate (vec3 (-1.0f, 1.0f, 0.0f))
	.m;
	
	glBindTexture (GL_TEXTURE_2D, *textures.font);
	
	const auto & shader = shaders.text;
	glUseProgram (*shader.program);
	glEnableVertexAttribArray (shader.vars.attr_pos);
	glEnableVertexAttribArray (shader.vars.attr_uv);
	set_pointers (shader.vars, line.verts.data ());
	
	const mat4 id (1.f);
	const vec4 body (1.f, 1.f, 1.f, 1.f);
	const vec4 outline (0.f, 0.f, 0.f, 0.75f);
	const vec4 bg (0.f, 0.f, 0.f, 0.f);
	
	glUniformMatrix4fv (shader.vars.uni_viewproj, 1, false, &mvp [0][0]);
	glUniformMatrix4fv (shader.vars.uni_model, 1, false, &id [0][0]);
	
	glUniform4fv (shader.vars.uni_body, 1, &body.r);
	glUniform4fv (shader.vars.uni_outline, 1, &outline.r);
	glUniform4fv (shader.vars.uni_bg, 1, &bg.r);
	
	glDrawElements (GL_TRIANGLES, line.indexes.size (), GL_UNSIGNED_SHORT, line.indexes.data ());
}

int Game::step (FileSystem & fs) {
	const auto frame_time = SDL_GetTicks ();
	
	const long elapsed = frame_time - prev_frame_time;
	
	const int steps = glm::min (3, timestep.consume (elapsed));
	
	for (int i = 0; i < steps; i++) {
		do_logic (fs);
		
		const bool on_last_step = i == steps - 1;
		if (on_last_step) {
			camera = camera.clamped ();
			
			// Rendering only gets triggered on the last frame, 
			// and only if the logic runs at all
			
			if (frames % 1 == 0) {
				const vec3 xs [6] = {
					vec3 (0,  0,  1),
					vec3 (0,  0,  -1),
					vec3 (1, 0,  0),
					vec3 (1, 0,  0),
					vec3 (-1,  0, 0),
					vec3 (1,  0,  0),
				};
				
				const vec3 ys [6] = {
					vec3 (0,  -1,  0),
					vec3 (0,  -1,  0),
					vec3 (0, 0,  1),
					vec3 (0,  0,  -1),
					vec3 ( 0,  -1, 0),
					vec3 ( 0,  -1, 0),
				};
				
				const vec3 zs [6] = {
					vec3 (1, 0,  0),
					vec3 (-1,  0,  0),
					vec3 ( 0, -1,  0),
					vec3 ( 0,  1,  0),
					vec3 ( 0,  0, 1),
					vec3 ( 0,  0,  -1),
				};
				
				glFrontFace (GL_CW);
				for (int i = 0; i < 6; i++) {
					rtt_fbo [i].bind ();
					glViewport (0, 0, 256, 256);
					const Transform envmap_viewproj = get_envmap_viewproj (zs [i], xs [i], ys [i]);
					render (envmap_viewproj, prop_pos);
				}
			}
			
			glFrontFace (GL_CCW);
			glViewport (0, 0, settings.width, settings.height);
			glBindFramebuffer (GL_FRAMEBUFFER, 0);
			const Transform viewproj = get_player_viewproj ();
			render (viewproj, camera_pos);
			render_ui ();
			
			sdl.swap_buffers ();
		}
	}
	
	prev_frame_time = frame_time;
	
	return steps;
}

