#include "include/audio.h"

#include "AL/alext.h"

using namespace std;

Audio::Audio () {
	const ALCint sample_rate = 48000;
	
	ALCint attrs [] = {
		ALC_FREQUENCY,
		sample_rate,
		ALC_FORMAT_CHANNELS_SOFT,
		ALC_STEREO_SOFT,
		ALC_FORMAT_TYPE_SOFT,
		ALC_SHORT_SOFT,
		0
	};
	
	dev = up_alc_device (alcOpenDevice (nullptr), [](ALCdevice * d) { alcCloseDevice (d); });
	ctx = up_alc_context (alcCreateContext (&*dev, attrs), [](ALCcontext * c) { 
		alcMakeContextCurrent (nullptr);
		alcDestroyContext (c);
	});
	alcMakeContextCurrent (&*ctx);
	
	alGenSources (1, &src_bgm);
	
	alGenBuffers (1, &buffer_test);
	
	vector <int16_t> pcm;
	pcm.resize (218 * 200);
	
	for (size_t i = 0; i < pcm.size (); i++) {
		pcm [i] = (i % 218) * 32768 / 218 - 16384;
	}
	
	alBufferData (buffer_test, AL_FORMAT_MONO16, pcm.data (), pcm.size () * sizeof (int16_t), sample_rate);
	
	alSourceQueueBuffers (src_bgm, 1, &buffer_test);
	alSourcePlay (src_bgm);
}

Audio::~Audio () {
	alDeleteBuffers (1, &buffer_test);
}
