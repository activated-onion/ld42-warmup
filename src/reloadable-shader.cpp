#include "reloadable-shader.h"

#include <iostream>

using namespace std;

void visit_GetUniformLocation (const std::string & name, int & n, Visitor & v) {
	// TODO: This is stupid
	if (name [0] == 'u') {
		const int uniform = *((GLuint *)v.userdata);
		n = glGetUniformLocation (uniform, name.c_str ());
	}
	else if (name [0] == 'a') {
		const int attr = *((GLuint *)v.userdata);
		
		n = glGetAttribLocation (attr, name.c_str ());
		glEnableVertexAttribArray (n);
	}
}
