uniform highp mat4 uni_light_mat;
uniform highp mat4 uni_viewproj;

uniform lowp float uni_fullbright;
uniform lowp float uni_flip_normals;
uniform lowp float uni_sky;

uniform highp vec3 uni_camera_pos;
uniform lowp vec3 uni_glow;

uniform lowp float uni_scale;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;
varying lowp float vary_fog;

lowp float gamma_light (lowp vec3 normal, lowp vec3 sun_vec) {
	lowp float d = dot (normal, sun_vec);
	
	lowp float sky = d * 0.5 + 0.5;
	lowp float sun = clamp (d, 0.0, 1.0);
	
	return sqrt (sky * 0.25 + sun * 0.75);
}

void main (void) {
	vary_uv = attr_uv;
	
	lowp vec4 light_color = vec4 (1.0);
	
	highp vec4 camera_pos = uni_viewproj * (attr_pos * vec4 (vec3 (uni_scale), 1.0));
	
	vec3 normal = attr_normal;
	if (uni_flip_normals == -1.0) {
		normal = -normal;
	}
	
	//vary_color = gamma_light (normal, vec3 (0.0625, -0.125, 1.0)) * light_color;
	
	highp vec3 camera_vec = (attr_pos.xyz * uni_scale) - (uni_light_mat * vec4 (uni_camera_pos, 1.0)).xyz;
	highp float light_distance = length (camera_vec);
	camera_vec = camera_vec / light_distance;
	vary_color = vec4 (1.0) * sqrt (max (0.0, 4.0 * abs (dot (-camera_vec, normal)) / light_distance));
	
	vary_color = max (vary_color, vec4 (uni_glow, 0.0));
	
	if (uni_fullbright == 1.0) {
		vary_color = vec4 (1.0);
	}
	
	if (uni_sky == 1.0) {
		camera_pos.z = 0.999 * camera_pos.w;
	}
	
	//vary_fog = clamp (sqrt (camera_pos.z / 128.0), 0.0, 1.0);
	vary_fog = 0.0;
	
	//vary_color.a = 0.5;
	
	gl_Position = camera_pos;
}
