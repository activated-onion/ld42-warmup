uniform lowp sampler2D uni_texture;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;

void main (void) {
	gl_FragColor = vec4 (vary_color.aaa, 1.0) * vary_color * texture2D (uni_texture, vary_uv);
	
	//gl_FragColor = vec4 (0.0, vary_color.a, 0.0, 1.0);
}
