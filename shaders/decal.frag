uniform lowp sampler2D uni_texture;
uniform lowp vec4 uni_tint;
uniform lowp vec3 uni_light_dir;
uniform lowp vec3 uni_sky_dir;
uniform lowp float uni_fullbright;

varying mediump vec2 vary_uv;
varying lowp vec3 vary_normal;

void main (void) {
	lowp vec3 sun = vec3 (0.4242, 0.4242, 0.4242);
	lowp vec3 sky = vec3 (0.1881, 0.2814, 0.2814);
	
	lowp float sun_power = max (0.0, dot (vary_normal, uni_light_dir));
	lowp float min_sky_power = 0.0625;
	lowp float max_sky_power = 1.0;
	lowp float sky_power = (dot (vary_normal, uni_sky_dir) * 0.5 + 0.5) * (max_sky_power - min_sky_power) + min_sky_power;
	
	lowp vec3 diffuse = max (vec3 (uni_fullbright), sqrt (sun * vec3 (sun_power) + sky * vec3 (sky_power)));
	
	lowp vec4 tex = texture2D (uni_texture, vary_uv);
	
	gl_FragColor = vec4 (tex.rgb * diffuse * uni_tint.rgb, tex.a * uni_tint.a);
	//gl_FragColor = vec4 (vary_uv.x, vary_uv.y, 0.5, 1.0);
	//gl_FragColor = vec4 (vary_normal, 1.0);
	//gl_FragColor = vec4 (uni_light_dir, 1.0);
}
