uniform lowp sampler2D uni_texture;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;
varying lowp float vary_fog;

void main (void) {
	lowp vec4 fog = vec4 (23.0 / 255.0, 0.0, 49.0 / 255.0, 1.0);
	
	gl_FragColor = mix (vary_color * texture2D (uni_texture, vary_uv), fog, vary_fog);
}
