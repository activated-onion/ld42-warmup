uniform highp mat4 uni_viewproj;
uniform highp mat4 uni_uv_mat;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

varying mediump vec2 vary_uv;
varying lowp vec3 vary_normal;

void main (void) {
	vary_uv = (uni_uv_mat * attr_pos).xy;
	vary_normal = attr_normal;
	gl_Position = uni_viewproj * vec4 (attr_pos.xyz, 1.0);
}
