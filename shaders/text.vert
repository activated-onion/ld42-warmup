attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
// Unused
attribute lowp vec3 attr_normal;

uniform highp mat4 uni_model;
uniform highp mat4 uni_viewproj;

varying mediump vec2 vary_uv;

void main () {
	gl_Position = uni_model * uni_viewproj * attr_pos;
	vary_uv = attr_uv;
}
