uniform lowp sampler2D uni_texture;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;

void main (void) {
	gl_FragColor = vary_color * vec4 (vec3 (1.0, 1.0, 1.0) * texture2D (uni_texture, vary_uv).rgb, 1.0);
	
	//gl_FragColor = vec4 (1.0, 0.0, 1.0, 1.0);
}
