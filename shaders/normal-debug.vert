uniform highp mat4 uni_model;
uniform highp mat4 uni_viewproj;

uniform lowp float uni_fullbright;
uniform lowp float uni_flip_normals;
uniform lowp float uni_sky;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;

lowp float gamma_light (lowp vec3 normal, lowp vec3 sun_vec) {
	lowp float d = dot (normal, sun_vec);
	
	lowp float sky = d * 0.5 + 0.5;
	lowp float sun = clamp (d, 0.0, 1.0);
	
	return sqrt (sky * 0.25 + sun * 0.75);
}

void main (void) {
	vary_uv = attr_uv;
	
	lowp vec4 light_color = vec4 (1.0);
	
	highp vec4 camera_pos = uni_viewproj * attr_pos;
	
	vec3 normal = (uni_model * vec4 (attr_normal, 0.0)).xyz;
	if (uni_flip_normals == -1.0) {
		normal = -normal;
	}
	
	vary_color = vec4 (sqrt (normal * vec3 (1.0, -1.0, 1.0)), 1.0);
	
	if (uni_fullbright == 1.0) {
		vary_color = vec4 (1.0);
	}
	
	if (uni_sky == 1.0) {
		camera_pos.z = 0.999 * camera_pos.w;
	}
	
	gl_Position = camera_pos;
}
