uniform highp vec4 uni_color;
uniform highp mat4 uni_model;
uniform highp mat4 uni_viewproj;
uniform lowp mat4 uni_particle_basis;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec4 attr_color;

varying lowp vec4 vary_color;
varying mediump vec2 vary_uv;

void main (void) {
	vary_uv = attr_uv;
	
	highp vec4 camera_pos = uni_viewproj * (attr_pos + uni_particle_basis * vec4 (attr_uv * 2.0 - vec2 (-1.0), 0.0, 0.0));
	
	vary_color = attr_color * uni_color;
	
	gl_Position = camera_pos;
}
