uniform sampler2D font;
uniform lowp vec4 uni_body; //= vec4 (0.75, 0.75, 0.75, 1.0);
uniform lowp vec4 uni_outline; // = vec4 (0.0, 0.0, 0.0, 0.5);
uniform lowp vec4 uni_bg; // = vec4 (0.0, 0.0, 0.0, 1.0);

varying mediump vec2 vary_uv;

void main () {
	lowp vec3 rgb = texture2D (font, vary_uv).rgb;
	lowp float bg = 1.0 - max (rgb.r, rgb.g);
	gl_FragColor = vec4 (rgb.g) * uni_body + vec4 (rgb.r) * uni_outline + vec4 (bg) * uni_bg;
	//gl_FragColor = vec4 (1.0, 0.0, 1.0, 1.0);
}
