uniform highp mat4 uni_bone_mats [64];
uniform highp mat4 uni_viewproj;
uniform highp mat4 uni_uv_mat;

attribute highp vec3 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;
attribute lowp vec4 attr_blend_index;
attribute lowp vec4 attr_blend_weight;

varying mediump vec2 vary_uv;
varying lowp vec3 vary_normal;
varying lowp vec4 vary_color;

void main (void) {
	highp vec4 model_pos = vec4 (attr_pos.xyz, 1.0);
	vary_uv = (uni_uv_mat * model_pos).xy;
	vary_normal = attr_normal;
	
	vary_color = attr_blend_weight;
	//vary_color = vec4 (vec3 (vary_normal.z * 0.5 + 0.5), 1.0);
	
	highp vec4 pos = vec4 (0.0, 0.0, 0.0, 0.0);
	
	for (int i = 0; i < 4; i++) {
		pos = pos + attr_blend_weight [i] * (uni_bone_mats [int (attr_blend_index [i])] * model_pos);
	}
	
	//pos = uni_bone_mats [int (attr_blend_index [0])] * model_pos;
	
	gl_Position = uni_viewproj * pos;
}
