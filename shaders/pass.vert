uniform highp mat4 uni_viewproj;

uniform mediump float uni_depth_adjust;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

varying mediump vec2 vary_uv;
varying lowp vec3 vary_normal;

void main (void) {
	vary_uv = attr_uv;
	vary_normal = attr_normal;
	gl_Position = uni_viewproj * vec4 (attr_pos.xyz, 1.0) + vec4 (0.0, 0.0, uni_depth_adjust, 0.0);
}
