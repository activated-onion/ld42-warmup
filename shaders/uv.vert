uniform highp vec2 uni_offset;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

void main (void) {
	gl_Position = vec4 ((attr_uv + uni_offset) * vec2 (2.0) - vec2 (1.0), 0.5, 1.0);
}
