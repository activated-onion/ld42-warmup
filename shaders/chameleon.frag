uniform lowp sampler2D uni_texture;

varying mediump vec2 vary_uv;
varying lowp vec3 vary_normal;

void main (void) {
	gl_FragColor = vec4 (texture2D (uni_texture, vary_uv).rgb, 1.0);
}
