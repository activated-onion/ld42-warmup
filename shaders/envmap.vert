uniform highp mat4 uni_viewproj;
uniform lowp vec3 uni_view_dir;

attribute highp vec4 attr_pos;
attribute mediump vec2 attr_uv;
attribute lowp vec3 attr_normal;

varying lowp vec3 vary_normal;
varying lowp vec3 vary_reflect_dir;

void main (void) {
	vary_normal = attr_normal;
	vary_reflect_dir = reflect (uni_view_dir, attr_normal);
	gl_Position = uni_viewproj * vec4 (attr_pos.xyz, 1.0);
}
