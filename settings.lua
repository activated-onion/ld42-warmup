return {
	settings = {
		title = "Office Demo",
		--draw_sky = true,
		draw_plane = false,
		pitch_speed = 1.0,
		roll_speed = 2.0,
		mouse_speed = 1.0,
		run_speed = 8.0,
		width = 800 * 1.5,
		height = 480 * 1.5,
		
		timestep_num = 60,
		timestep_den = 1000,
		
		mob_scale = 2.0,
	},
	
	textures = {
		alison = "textures/alison.png",
		font = "textures/font.png",
		louisa = "textures/louisa.png",
		pathos = "textures/rocket blast.png",
		shadow = "textures/shadow.png",
		white = "textures/white.png",
		walls = "textures/walls.png",
	},
	
	meshes = {
		bomb = "meshes/bomb.iqm",
		level = "meshes/office.iqm",
		mob = "meshes/mob.iqm",
		prop = "meshes/sphere.iqm",
		xy_plane = "meshes/xy-plane.iqm",
	},
	
	shaders = {
		diffuse = "shaders/diffuse",
		envmap = "shaders/envmap",
		particle = "shaders/particle",
		pass = "shaders/pass",
		text = "shaders/text",
		uv = "shaders/uv",
		decal = "shaders/decal",
	},
	
	sounds = {
	},
}
