#include "file-system.h"

#include <fstream>

using namespace std;

vector <uint8_t> FileSystem::load_file (const string & fn) const {
	ifstream f (fn.c_str (), ifstream::binary);
	
	f.seekg (0, ifstream::end);
	const auto size = f.tellg ();
	f.seekg (0, ifstream::beg);
	
	if (size > 0) {
		vector <uint8_t> buffer (size);
		
		f.read ((char *)buffer.data (), size);
		f.close ();
		
		return buffer;
	}
	return vector <uint8_t> ();
}
