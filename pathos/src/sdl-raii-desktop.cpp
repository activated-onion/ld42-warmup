#include "sdl-raii.h"

#include <SDL2/SDL.h>
#include "pathos-gl.h"

void close_sdl2 (SDL_Window * window) {
	SDL_DestroyWindow (window);
}

SdlRaii::SdlRaii (int w, int h) {
	SDL_Init (SDL_INIT_VIDEO);
	
	SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8);
	
	auto window = SDL_CreateWindow (
		"", 
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		w, h, SDL_WINDOW_OPENGL);
	SDL_GL_CreateContext (window);
	
#ifdef PATHOS_WIN32
	glewInit ();
#else
	GLeeInit ();
#endif
	
	sdl = up_sdl2 (window, [](SDL_Window * surf) { close_sdl2 (surf); });
}

void SdlRaii::swap_buffers () const {
	SDL_GL_SwapWindow (&*sdl);
}
