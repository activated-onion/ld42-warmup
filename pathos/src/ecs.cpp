#include "ecs.h"

#include <iostream>
using namespace std;
/*
void ComponentEraser::erase (Entity e) const {
	func_erase (opaque_component, e);
}
*/
/*
void EraserObserver::register_eraser (ComponentEraser ce) {
	erasers.push_back (ce);
}

void EraserObserver::erase (Entity e) const {
	cout << "EraserObserver " << e << endl;
	for (const auto & eraser: erasers) {
		eraser.erase (e);
	}
}
*/
EntityNamespace::EntityNamespace (): next_entity (0) {
	
}

Entity EntityNamespace::name_entity () {
	const auto e = next_entity;
	next_entity++;
	return e;
}
