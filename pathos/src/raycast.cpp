#include "raycast.h"

#include <algorithm>
#include <iostream>

#include "mesh.h"

using namespace glm;
using namespace std;

vector <int32_t> find_walkable_tris (const MeshBuffer & mesh, float floor_thresh) {
	vector <int32_t> result;
	
	for (uint tri_i = 0; tri_i < mesh.indexes.size (); tri_i += 3) {
		vec3 verts [3];
		vec3 norms [3];
		
		for (int j = 0; j < 3; j++) {
			const auto index = mesh.indexes [tri_i + j];
			const auto v = mesh.verts [index];
			
			verts [j] = vec3 (v.pos);
			norms [j] = v.normal;
			if (norms [j].z < floor_thresh) {
				goto skip_triangle;
			}
		}
		
		for (int j = 0; j < 3; j++) {
			result.push_back (mesh.indexes [tri_i + j]);
		}
		
		skip_triangle:
		;
	}
	
	return result;
}

Aabb2 find_corners (const MeshBuffer & mesh, const vector <int32_t> & tris) {
	bool first = true;
	Aabb2 result;
	
	for (int32_t mesh_index: tris) {
		const auto pos = vec2 (mesh.verts [mesh_index].pos);
		
		if (first) {
			first = false;
			result.min = result.max = pos;
		}
		else {
			result.min = min (result.min, pos);
			result.max = max (result.max, pos);
		}
	}
	
	return result;
}

ivec2 RaycastGrid::bin_point (vec2 xy) const {
	return ivec2 (
		floor ((xy.x - aabb.min.x) / grid_width),
		floor ((xy.y - aabb.min.y) / grid_width)
	);
}

iAabb2 RaycastGrid::bin_points (const MeshBuffer & mesh, const int32_t * indices, int32_t num_indices) const {
	iAabb2 bins;
	
	for (int i = 0; i < num_indices; i++) {
		const MeshVertex & vert = mesh.verts [indices [i]];
		
		const ivec2 vert_bin = bin_point (vec2 (vert.pos));
		
		if (i == 0) {
			bins.min = bins.max = vert_bin;
		}
		else {
			bins.min = min (bins.min, vert_bin);
			bins.max = max (bins.max, vert_bin);
		}
	}
	
	return bins;
}

int32_t RaycastGrid::get_bin_index (ivec2 xy) const {
	return xy.y * grid_stride + xy.x;
}

bool GridTri::operator < (const GridTri & b) {
	return grid_index < b.grid_index;
}

RaycastGrid make_grid (
	const MeshBuffer & mesh, 
	const vector <int32_t> & walkable_tris, 
	float grid_width
) {
	RaycastGrid grid;
	
	grid.grid_width = grid_width;
	grid.aabb = find_corners (mesh, walkable_tris);
	grid.grid_stride = ceil ((grid.aabb.max.x - grid.aabb.min.x) / grid.grid_width);
	
	for (int i = 0; i < walkable_tris.size (); i += 3) {
		const int32_t tri_indices [3] = {
			walkable_tris [i + 0],
			walkable_tris [i + 1],
			walkable_tris [i + 2],
		};
		
		const auto tri_bin = grid.bin_points (mesh, tri_indices, 3);
		
		for (int y = tri_bin.min.y; y <= tri_bin.max.y; y++) {
			for (int x = tri_bin.min.x; x <= tri_bin.max.x; x++) {
				int i = grid.get_bin_index (ivec2 (x, y));
				
				GridTri gv;
				gv.grid_index = i;
				for (int j = 0; j < 3; j++) {
					gv.mesh_tri [j] = tri_indices [j];
				}
				
				grid.tris.push_back (gv);
			}
		}
	}
	
	/* grid_tris should now contain a list of triangles and which grid bins
	 * they belong in. There will be more triangles here than in the original
	 * mesh, due to triangles that straddle borders. For this reason it is
	 * preferable to use as large a grid_width as possible.
	 */
	
	std::sort (grid.tris.begin (), grid.tris.end ());
	
	int32_t last_bin = grid.get_bin_index (grid.bin_point (grid.aabb.max));
	
	int32_t tri_index = 0;
	for (int current_bin = 0; current_bin < last_bin; current_bin++) {
		RaycastGridBin bin;
		bin.start = 0;
		bin.num_tris = 0;
		
		while (true) {
			const int32_t tri_bin = grid.tris [tri_index].grid_index;
			
			if (tri_bin == current_bin) {
				if (bin.num_tris == 0) {
					bin.num_tris = 1;
					bin.start = tri_index;
				}
				else {
					bin.num_tris++;
				}
				tri_index++;
			}
			else if (tri_bin < current_bin) {
				cerr << "GRID CONSTRUCTION FUCKED UP" << endl;
				break;
			}
			else {
				grid.grid_indices.push_back (bin);
				break;
			}
		}
	}
	
	return grid;
}

vec3 flatten (vec3 v) {
	return vec3 (v.x, v.y, 0.f);
}

float interpolate_z (vec3 verts [3], vec3 xyz) {
	float bary [3];
	
	for (int j = 0; j < 3; j++) {
		auto a = verts [(j + 0) % 3];
		auto b = verts [(j + 1) % 3];
		auto c = verts [(j + 2) % 3];
		
		const auto bary_axis = cross (vec3 (0, 0, 1), flatten (b - c));
		const float bary_a = dot (flatten (a), bary_axis);
		const float bary_bc = dot (flatten (b), bary_axis);
		const float bary_xy = dot (flatten (xyz), bary_axis);
		
		bary [j] = (bary_xy - bary_bc) / (bary_a - bary_bc);
	}
	
	return
		verts [0].z * bary [0] +
		verts [1].z * bary [1] +
		verts [2].z * bary [2];
}

Raycast2d raycast (const MeshBuffer & mesh, const vector <int32_t> & walkable_tris, vec3 xyz) {
	Raycast2d result;
	result.hit = false;
	
	// Kill-Z
	result.z = 0.f;
	
	for (uint tri_i = 0; tri_i < walkable_tris.size (); tri_i += 3) {
		vec3 verts [3];
		
		for (int j = 0; j < 3; j++) {
			const auto index = walkable_tris [tri_i + j];
			const auto v = mesh.verts [index];
			
			verts [j] = vec3 (v.pos);
		}
		
		for (int j = 0; j < 3; j++) {
			const auto a = verts [(j + 0) % 3];
			const auto b = verts [(j + 1) % 3];
			
			const bool inside_edge = cross (a - b, xyz - b).z > 0.f;
			
			if (! inside_edge) {
				goto skip_triangle;
			}
		}
		
		result.hit = true;
		result.z = interpolate_z (verts, xyz);
		
		skip_triangle:
		;
	}
	
	return result;
}

Raycast2d raycast_grid (const MeshBuffer & mesh, const RaycastGrid & grid, vec3 xyz) {
	Raycast2d result;
	result.hit = false;
	
	// Kill-Z
	result.z = 0.f;
	
	const RaycastGridBin & bin = grid.grid_indices [grid.get_bin_index (grid.bin_point (vec2 (xyz)))];
	
	for (int32_t tri_i = bin.start; tri_i < bin.start + bin.num_tris; tri_i++) {
		vec3 verts [3];
		
		for (int j = 0; j < 3; j++) {
			const auto index = grid.tris [tri_i].mesh_tri [j];
			const auto v = mesh.verts [index];
			
			verts [j] = vec3 (v.pos);
		}
		
		for (int j = 0; j < 3; j++) {
			const auto a = verts [(j + 0) % 3];
			const auto b = verts [(j + 1) % 3];
			
			const bool inside_edge = cross (a - b, xyz - b).z > 0.f;
			
			if (! inside_edge) {
				goto skip_triangle;
			}
		}
		
		result.hit = true;
		result.z = interpolate_z (verts, xyz);
		
		skip_triangle:
		;
	}
	
	return result;
}
