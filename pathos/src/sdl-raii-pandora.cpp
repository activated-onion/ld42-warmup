#include "sdl-raii.h"

#include <SDL/SDL.h>
#include "pathos-gl.h"

void close_sdl1 (SDL_Surface *) {
	EGL_Close ();
	
	SDL_ShowCursor (true);
	SDL_WM_GrabInput (SDL_GRAB_OFF);
	SDL_Quit ();
}

SdlRaii::SdlRaii (int, int) {
	SDL_Init (SDL_INIT_VIDEO);
	
	SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8);
	const auto flags = SDL_SWSURFACE | SDL_FULLSCREEN;
	SDL_GL_SetAttribute (SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute (SDL_GL_MULTISAMPLESAMPLES, 4);
	
	// Pandora cannot do windowed mode - Fixed at 800x480
	const auto screen = SDL_SetVideoMode (800, 480, 0, flags);
	
	EGL_Open (800, 480);
	
	sdl = up_sdl1 (screen, [](SDL_Surface * surf) { close_sdl1 (surf); });
}

void SdlRaii::swap_buffers () const {
	EGL_SwapBuffers ();
}
