#include "text.h"

using namespace glm;
using namespace std;

vector <TextVertex> make_square () {
	vector <TextVertex> square {
		{ vec4 (0, 0, 0, 1), vec2 (0, 0) },
		{ vec4 (1, 0, 0, 1), vec2 (1, 0) },
		{ vec4 (1, 1, 0, 1), vec2 (1, 1) },
		{ vec4 (0, 1, 0, 1), vec2 (0, 1) },
	};
	
	return square;
}

Text layout_text (const string & s) {
	vec2 rect [] {
		vec2 (0, 0),
		vec2 (1, 0),
		vec2 (1, 1),
		vec2 (0, 1),
	};
	
	uint16_t rect_indices [] {
		0, 1, 2,
		0, 2, 3,
	};
	
	Text result;
	result.width = s.size ();
	result.height = 1;
	
	for (size_t x = 0; x < s.size (); x++) {
		const char c = s [x];
		
		vec2 uv (c % 16, c / 16);
		
		for (int i = 0; i < 4; i++) {
			const vec2 v = rect [i];
			
			result.verts.push_back (TextVertex {
				vec4 ((x + v.x) * 10, v.y * 12, 0, 1),
				(uv + v) * vec2 (10, 12) / vec2 (256),
			});
		}
		
		// Is it even worth it to use indices for text?
		for (int i = 0; i < 6; i++) {
			result.indices.push_back (x * 4 + rect_indices [i]);
		}
	}
	
	return result;
}

MeshBuffer layout_text_mesh (const string & s) {
	vec2 rect [] {
		vec2 (0, 0),
		vec2 (1, 0),
		vec2 (1, 1),
		vec2 (0, 1),
	};
	
	uint16_t rect_indices [] {
		0, 2, 1,
		0, 3, 2,
	};
	
	MeshBuffer result;
	
	int x = 0;
	int y = 0;
	int k = 0;
	for (size_t j = 0; j < s.size (); j++) {
		const char c = s [j];
		switch (c) {
			case '\n':
				x = 0;
				y++;
				break;
			case '\r':
				
				break;
			default:
			{
			vec2 uv (c % 16, c / 16);
			
			for (int i = 0; i < 4; i++) {
				const vec2 v = rect [i];
				
				result.verts.push_back (MeshVertex {
					vec4 ((x + v.x) * 10, (y + v.y) * 12, 0, 1),
					(uv + v) * vec2 (10, 12) / vec2 (256),
					vec3 (),
				});
			}
			
			// Is it even worth it to use indices for text?
			for (int i = 0; i < 6; i++) {
				result.indexes.push_back (k * 4 + rect_indices [i]);
			}
			x++;
			k++;
			}
		}
	}
	
	return result;
}
