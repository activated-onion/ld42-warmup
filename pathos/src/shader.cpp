#include "shader.h"

#include <string>

using namespace std;

ShaderRaii::ShaderRaii (GLenum type) {
	id = glCreateShader (type);
}

ShaderRaii::~ShaderRaii () {
	glDeleteShader (id);
}

void ShaderRaii::source (const std::vector <uint8_t> & source) const {
#ifdef INSID_PANDORA
	const string header ("#version 100\n#line 0\n");
#else
	const string header ("#define lowp\n#define mediump\n#define highp\n\n#line 0\n");
#endif
	
	const int NUM_SOURCES = 2;
	const char * sources [NUM_SOURCES] = {
		header.c_str (),
		(const char *)source.data (),
	};
	
	const int lengths [NUM_SOURCES] = {
		(int)header.size (),
		(int)source.size (),
	};
	
	glShaderSource (id, NUM_SOURCES, sources, lengths);
}

GLuint ShaderRaii::operator * () const {
	return id;
}

ShaderProgramRaii::ShaderProgramRaii () {
	id = glCreateProgram ();
}

ShaderProgramRaii::~ShaderProgramRaii () {
	glDeleteProgram (id);
}

GLuint ShaderProgramRaii::operator * () const {
	return id;
}
