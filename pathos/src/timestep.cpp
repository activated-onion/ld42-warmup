#include "timestep.h"

Timestep::Timestep () :
	accumulator (0),
	num (1),
	den (1)
{}

Timestep::Timestep (int n, int d) {
	accumulator = 0;
	num = n;
	den = d;
}

int Timestep::consume (int d) {
	accumulator += d * num;
	
	int steps = 0;
	while (accumulator > 0) {
		accumulator -= den;
		steps++;
	}
	return steps;
}
