#include "texture.h"

TextureRaii::TextureRaii () {
	id = 0;
	glGenTextures (1, &id);
}

TextureRaii::~TextureRaii () {
	if (id != 0) {
		glDeleteTextures (1, &id);
	}
}

TextureRaii::TextureRaii (TextureRaii && o): id (0) {
	id = o.id;
	o.id = 0;
}

GLuint TextureRaii::operator * () const {
	return id;
}

void TextureRaii::bind () const {
	glBindTexture (GL_TEXTURE_2D, id);
}
