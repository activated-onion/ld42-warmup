#include "mesh.h"

#include <iostream>
#include <stdint.h>

#include "iqm.h"

using namespace std;

template <typename TVertex>
vector <TVertex> load_mesh_vertices (const vector <uint8_t> & bytes, const int32_t * array_offsets) {
	const auto header = (const iqmheader *)&bytes [0];
	const auto vertexarrays = (const iqmvertexarray *)&bytes [header->ofs_vertexarrays];
	
	const int32_t num_scalars = sizeof (TVertex) / sizeof (float);
	
	vector <TVertex> vert_buffer;
	vert_buffer.resize (header->num_vertexes);
	auto float_buffer = (float *)vert_buffer.data ();
	for (int i = 0; i < (int)header->num_vertexarrays; i++) {
		const auto & vertexarray = vertexarrays [i];
		
		if (vertexarray.type > 6) {
			continue;
		}
		
		const int32_t index_offset = array_offsets [vertexarray.type];
		
		if (false && vertexarray.type == IQM_POSITION) {
			cerr << "position size = " << vertexarray.size << endl;
		}
		
		if (index_offset < 0) {
			const auto & va = vertexarray;
			if (false) {
				cerr << "vertexarray " << i << endl;
				cerr << "type " << va.type << endl;
				cerr << "format " << va.format << endl;
				cerr << "size " << va.size << endl;
			}
			continue;
		}
		auto scalars = (const float *)&bytes [vertexarray.offset];
		
		for (int j = 0; j < (int)header->num_vertexes; j++) {
			for (int k = 0; k < (int)vertexarray.size; k++) {
				float_buffer [j * num_scalars + index_offset + k] = scalars [vertexarray.size * j + k];
			}
		}
	}
	
	return vert_buffer;
}

vector <SkinnedMeshVertex> load_skinned_mesh_vertices (const vector <uint8_t> & bytes) 
{
	const auto header = (const iqmheader *)&bytes [0];
	const auto vertexarrays = (const iqmvertexarray *)&bytes [header->ofs_vertexarrays];
	
	vector <SkinnedMeshVertex> vert_buffer;
	vert_buffer.resize (header->num_vertexes);
	
	uint32_t pos_offset = -1;
	uint32_t uv_offset = -1;
	uint32_t normal_offset = -1;
	uint32_t blend_index_offset = -1;
	uint32_t blend_weight_offset = -1;
	
	for (int i = 0; i < (int)header->num_vertexarrays; i++) {
		const auto & va = vertexarrays [i];
		const auto offset = va.offset;
		
		switch (va.type) {
			case IQM_POSITION:
				pos_offset = offset;
				break;
			case IQM_TEXCOORD:
				uv_offset = offset;
				break;
			case IQM_NORMAL:
				normal_offset = offset;
				break;
			case IQM_BLENDINDEXES:
				blend_index_offset = offset;
				break;
			case IQM_BLENDWEIGHTS:
				blend_weight_offset = offset;
				break;
		}
	}
	
	const auto * pos_buffer = (const float *)&bytes [pos_offset];
	const auto * uv_buffer = (const float *)&bytes [uv_offset];
	const auto * normal_buffer = (const float *)&bytes [normal_offset];
	const auto * blend_index_buffer = (const uint8_t *)&bytes [blend_index_offset];
	const auto * blend_weight_buffer = (const uint8_t *)&bytes [blend_weight_offset];
	
	for (size_t i = 0; i < vert_buffer.size (); i++) {
		auto & vert = vert_buffer [i];
		
		vert.pos.x = pos_buffer [i * 3 + 0];
		vert.pos.y = pos_buffer [i * 3 + 1];
		vert.pos.z = pos_buffer [i * 3 + 2];
		
		vert.uv.x = uv_buffer [i * 2 + 0];
		vert.uv.y = uv_buffer [i * 2 + 1];
		
		vert.normal.x = normal_buffer [i * 3 + 0];
		vert.normal.y = normal_buffer [i * 3 + 1];
		vert.normal.z = normal_buffer [i * 3 + 2];
		
		for (int j = 0; j < 4; j++) {
			vert.blend_index [j] = blend_index_buffer [i * 4 + j];
			vert.blend_weight [j] = blend_weight_buffer [i * 4 + j];
		}
	}
	
	return vert_buffer;
}

vector <uint16_t> load_mesh_indexes (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	const auto triangles = (const iqmtriangle *)&bytes [header->ofs_triangles];
	vector <uint16_t> index_buffer;
	index_buffer.reserve (header->num_triangles * 3);
	for (int i = 0; i < (int)header->num_triangles; i++) {
		for (int j = 0; j < 3; j++) {
			index_buffer.push_back (triangles [i].vertex [j]);
		}
	}
	
	return index_buffer;
}

string get_text (const vector <uint8_t> & bytes, uint32_t offset) {
	const auto header = (const iqmheader *)&bytes [0];
	
	return string ((const char *)&bytes [header->ofs_text + offset]);
}

vector <MeshSlice> load_mesh_slices (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	const auto meshes = (const iqmmesh *)&bytes [header->ofs_meshes];
	vector <MeshSlice> mesh_buffer;
	mesh_buffer.resize (header->num_meshes);
	for (int i = 0; i < (int)mesh_buffer.size (); i++) {
		const auto & mesh = meshes [i];
		mesh_buffer [i] = {
			(uint16_t)(mesh.first_triangle * 3),
			(uint16_t)(mesh.num_triangles * 3),
			get_text (bytes, mesh.name),
		};
	}
	
	return mesh_buffer;
}

MeshBuffer MeshBuffer::load_iqm (const vector <uint8_t> & bytes) {
	if (bytes.size () < sizeof (iqmheader)) {
		return MeshBuffer ();
	}
	
	static const int32_t array_offsets [] = {
		0,
		4,
		6,
		-1, 
		-1,
		-1,
		-1,
	};
	
	const auto vert_buffer = load_mesh_vertices <MeshVertex> (bytes, array_offsets);
	const auto index_buffer = load_mesh_indexes (bytes);
	const auto mesh_slices = load_mesh_slices (bytes);
	
	return MeshBuffer {
		vert_buffer,
		index_buffer,
		mesh_slices,
	};
}

vector <Pose> load_poses (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	
	vector <Pose> pose_buffer;
	pose_buffer.resize (header->num_poses);
	
	const auto poses = (const iqmpose *)&bytes [header->ofs_poses];
	
	for (uint32_t i = 0; i < header->num_poses; i++) {
		const auto & pose = poses [i];
		auto & out_pose = pose_buffer [i];
		
		if (false) {
			cerr << "Pose " << i << endl;
			cerr << " parent " << pose.parent << endl;
			cerr << " channelmask " << pose.mask << endl;
		}
		
		if (pose.parent >= i) {
			cerr << "Warning: Bone " << i << " comes before parent " << pose.parent << endl;
		}
		
		out_pose.parent = pose.parent;
		out_pose.mask = pose.mask;
		
		for (size_t j = 0; j < 10; j++) {
			if (false) {
				cerr << " offset, scale = " << pose.channeloffset [j] << ", " << pose.channelscale [j] << endl;
			}
			
			out_pose.channeloffset [j] = pose.channeloffset [j];
			out_pose.channelscale [j] = pose.channelscale [j];
		}
	}
	
	return pose_buffer;
}

vector <Anim> load_anims (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	
	vector <Anim> anim_buffer;
	anim_buffer.resize (header->num_anims);
	
	const auto anims = (const iqmanim *)&bytes [header->ofs_anims];
	
	for (uint32_t i = 0; i < header->num_anims; i++) {
		const auto & anim = anims [i];
		auto & out_anim = anim_buffer [i];
		
		if (false) {
			cerr << "Anim " << i << endl;
			cerr << " name " << get_text (bytes, anim.name) << endl;
			cerr << " first_frame, num_frames = " << anim.first_frame << ", " << anim.num_frames << endl;
			cerr << " framerate " << anim.framerate << endl;
			cerr << " flags " << anim.flags << endl;
		}
		
		out_anim.name = get_text (bytes, anim.name);
		out_anim.first_frame = anim.first_frame;
		out_anim.num_frames = anim.num_frames;
		out_anim.framerate = anim.framerate;
		out_anim.flags = anim.flags;
	}
	
	return anim_buffer;
}

vector <uint16_t> load_frames (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	
	const auto frames = (const uint16_t *)&bytes [header->ofs_frames];
	
	vector <uint16_t> frame_buffer (&frames [0], &frames [header->num_frames * header->num_framechannels]);
	
	return frame_buffer;
}

vector <Joint> load_joints (const vector <uint8_t> & bytes) {
	const auto header = (const iqmheader *)&bytes [0];
	const auto joints = (const iqmjoint *)&bytes [header->ofs_joints];
	
	vector <Joint> joint_buffer;
	joint_buffer.resize (header->num_joints);
	
	for (uint32_t i = 0; i < header->num_joints; i++) {
		const auto & joint = joints [i];
		
		auto & out_joint = joint_buffer [i];
		out_joint.name = get_text (bytes, joint.name);
		out_joint.parent = joint.parent;
		
		out_joint.translate.x = joint.translate [0];
		out_joint.translate.y = joint.translate [1];
		out_joint.translate.z = joint.translate [2];
		
		out_joint.rotate.x = joint.rotate [0];
		out_joint.rotate.y = joint.rotate [1];
		out_joint.rotate.z = joint.rotate [2];
		out_joint.rotate.w = joint.rotate [3];
		
		out_joint.scale.x = joint.scale [0];
		out_joint.scale.y = joint.scale [1];
		out_joint.scale.z = joint.scale [2];
		
		if (true) {
			cerr << "Joint \"" << get_text (bytes, joint.name) << "\"" << endl;
			cerr << " parent " << joint.parent << endl;
			
			const auto & tr = out_joint.translate;
			const auto & r = out_joint.rotate;
			
			cerr << " translate " << tr.x << ", " << tr.y << ", " << tr.z << endl;
			cerr << " rotate " << r.x << ", " << r.y << ", " << r.z << ", " << r.w << endl;
		}
	}
	
	if (true) {
		cerr << "num_poses " << header->num_poses << endl;
		cerr << "num_anims " << header->num_anims << endl;
		cerr << "num_frames " << header->num_frames << endl;
		cerr << "num_framechannels " << header->num_framechannels << endl;
	}
	
	return joint_buffer;
}

SkinnedMeshBuffer SkinnedMeshBuffer::load_iqm (const vector <uint8_t> & bytes) {
	if (bytes.size () < sizeof (iqmheader)) {
		return SkinnedMeshBuffer ();
	}
	
	const auto header = (const iqmheader *)&bytes [0];
	
	const auto joint_buffer = load_joints (bytes);
	const auto vert_buffer = load_skinned_mesh_vertices (bytes);
	const auto index_buffer = load_mesh_indexes (bytes);
	const auto mesh_slices = load_mesh_slices (bytes);
	
	return SkinnedMeshBuffer {
		vert_buffer,
		index_buffer,
		mesh_slices,
		joint_buffer,
		load_poses (bytes),
		header->num_framechannels,
		load_frames (bytes),
	};
}
