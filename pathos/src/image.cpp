#include "image.h"

#include <string.h>
#include <iostream>
#include "stb_image.h"

using namespace std;

Image::Image ():
	w (0),
	h (0),
	channels (0)
	{}

Image::Image (const vector <uint8_t> & compressed) {
	w = 0;
	h = 0;
	
	unsigned char * bytes = stbi_load_from_memory (
		(const unsigned char *)compressed.data (),
		compressed.size (),
		&w, &h, &channels, 0);
	
	if (bytes != nullptr) {
		pixels.resize (w * h * channels);
		memcpy (pixels.data (), bytes, pixels.size ());
		stbi_image_free (bytes);
	}
}

Image::~Image () {
	
}
