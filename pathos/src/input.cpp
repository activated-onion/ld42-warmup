#include "input.h"

#include <SDL2/SDL.h>

using namespace glm;

Buttons::Buttons () {
	memset (btns, 0, sizeof (btns));
}

bool Buttons::get (EButton b) const {
	return btns [(int)b];
}

EButton Buttons::map (SDL_Scancode key) {
	switch (key) {
		case SDL_SCANCODE_UP:
		case SDL_SCANCODE_W:
		case SDL_SCANCODE_I:
		case SDL_SCANCODE_PAGEUP:
			return EButton::Up;
		case SDL_SCANCODE_DOWN:
		case SDL_SCANCODE_S:
		case SDL_SCANCODE_K:
		case SDL_SCANCODE_PAGEDOWN:
			return EButton::Down;
		case SDL_SCANCODE_LEFT:
		case SDL_SCANCODE_A:
		case SDL_SCANCODE_J:
		case SDL_SCANCODE_HOME:
			return EButton::Left;
		case SDL_SCANCODE_RIGHT:
		case SDL_SCANCODE_D:
		case SDL_SCANCODE_L:
		case SDL_SCANCODE_END:
			return EButton::Right;
		case SDL_SCANCODE_P:
			return EButton::Pause;
		case SDL_SCANCODE_RCTRL:
			return EButton::Click;
		case SDL_SCANCODE_SPACE:
			return EButton::Jump;
		case SDL_SCANCODE_Z:
			return EButton::Sink;
		default:
			return EButton::Nil;
	}
}

vec2 get_move_vec (const Buttons & btns_held) {
	vec2 input_vec (0.f);
	
	if (btns_held.get (EButton::Up)) {
		input_vec += vec2 (0, 1);
	}
	if (btns_held.get (EButton::Down)) {
		input_vec += vec2 (0, -1);
	}
	if (btns_held.get (EButton::Left)) {
		input_vec += vec2 (-1, 0);
	}
	if (btns_held.get (EButton::Right)) {
		input_vec += vec2 (1, 0);
	}
	
	if (length (input_vec) > 1.f) {
		input_vec = normalize (input_vec);
	}
	
	return input_vec;
}

Buttons get_kbd_buttons () {
	const auto key_state = SDL_GetKeyboardState (nullptr);
	Buttons btns_held;
	
	for (int i = 0; i < SDL_NUM_SCANCODES; i++) {
		btns_held.btns [(int)Buttons::map ((SDL_Scancode)i)] |= key_state [i];
	}
	
	return btns_held;
}

Buttons get_joy_buttons (SDL_Joystick * joy) {
	Buttons btns_held;
	
	if (joy == nullptr) {
		return btns_held;
	}
	
	const auto hat_stat = SDL_JoystickGetHat (joy, 0);
	if (hat_stat & SDL_HAT_UP) {
		btns_held.btns [(int)EButton::Up] = true;
	}
	if (hat_stat & SDL_HAT_DOWN) {
		btns_held.btns [(int)EButton::Down] = true;
	}
	if (hat_stat & SDL_HAT_LEFT) {
		btns_held.btns [(int)EButton::Left] = true;
	}
	if (hat_stat & SDL_HAT_RIGHT) {
		btns_held.btns [(int)EButton::Right] = true;
	}
	
	return btns_held;
}

Buttons or_buttons (const Buttons & a, const Buttons & b) {
	Buttons result;
	
	for (int i = 0; i < (int)EButton::COUNT; i++) {
		result.btns [i] = a.btns [i] || b.btns [i];
	}
	
	return result;
}
