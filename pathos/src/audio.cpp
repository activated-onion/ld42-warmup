#include "audio.h"

#include <iostream>
#include <string>

#include "file-system.h"

using namespace std;

ALenum PcmBuffer::openal_fmt () const {
	switch (nchannels) {
		case 1:
			return AL_FORMAT_MONO16;
		default:
			return AL_FORMAT_STEREO16;
	}
}

void PcmBuffer::to_openal (ALuint b) const {
	const ALenum fmt = openal_fmt ();
	alBufferData (b, fmt, &buffer [0], buffer.size () * sizeof (int16_t), sample_rate);
}
/*
VorbisDec::VorbisDec (const vector <uint8_t> b): buffer (b) {
	ov_handle = unique_ptr <OggVorbis_File, function <void (OggVorbis_File *)> > (new OggVorbis_File (), ov_clear);
	
	ov_callbacks cb;
	cb.close_func = nullptr;
	cb.read_func = vorb_read;
	cb.seek_func = vorb_seek;
	cb.tell_func = vorb_tell;
	
	cursor = 0;
	
	ov_open_callbacks (this, &*ov_handle, nullptr, 0, cb);
}

int VorbisDec::read (int16_t * buf, int length) {
	int bitstream = 0;
	const long rc = ov_read (&*ov_handle, (char *)buf, length, &bitstream);
	if (rc > 0) {
		return rc / sizeof (int16_t);
	}
	else {
		return 0;
	}
}

PcmBuffer VorbisDec::decode_all (const vector <uint8_t> & vorbis_data) {
	VorbisDec dec (vorbis_data);
	PcmBuffer result;
	
	const auto * info = ov_info (&*(dec.ov_handle), -1);
	result.nchannels = info->channels;
	result.sample_rate = (int)info->rate;
	
	const ogg_int64_t pcm_len = ov_pcm_total (&*(dec.ov_handle), -1);
	
	result.buffer.resize (pcm_len * result.nchannels);
	
	ogg_int64_t cursor = 0;
	
	while (true) {
		const int samples_read = dec.read (&result.buffer [cursor], result.buffer.size () - cursor);
		
		cursor += samples_read;
		
		if (samples_read <= 0) {
			break;
		}
	}
	
	return result;
}

size_t vorb_read (void * ptr, size_t size, size_t nmemb, void * datasource) {
	auto that = (VorbisDec *)datasource;
	
	int remaining = that->buffer.size () - that->cursor;
	int requested = size * nmemb;
	
	const auto to_read = std::min (remaining, requested);
	
	copy (&(that->buffer [that->cursor]), &(that->buffer [that->cursor + to_read]), (uint8_t *)ptr);
	
	that->cursor += to_read;
	
	return to_read;
}

int vorb_seek (void * datasource, int64_t offset, int whence) {
	auto * that = (VorbisDec *)datasource;
	
	switch (whence) {
		case SEEK_SET:
			that->cursor = offset;
			break;
		case SEEK_CUR:
			that->cursor += offset;
			break;
		case SEEK_END:
			that->cursor = that->buffer.size () + offset;
			break;
	}
	
	return 0;
}

long vorb_tell (void * datasource) {
	return ((VorbisDec *)datasource)->cursor;
}
*/

void vorb_to_buffer_stb (ALuint & buffer, const FileSystem & fs, string fn) {
	buffer = 0;
	alGenBuffers (1, &buffer);
	
	const auto vorbis = fs.load_file (fn);
	int channels = 0;
	int sample_rate = 0;
	int16_t * pcm = nullptr;
	int num_samples = stb_vorbis_decode_memory (vorbis.data (), vorbis.size (), &channels, &sample_rate, &pcm);
	
	alBufferData (buffer, AL_FORMAT_MONO16, pcm, num_samples * channels * sizeof (int16_t), sample_rate);
	
	//cerr << "Loaded " << fn << " = " << pcm << endl;
	
	if (pcm != nullptr) {
		free (pcm);
	}
}

/*
void vorb_to_buffer_tremor (ALuint & buffer, const FileSystem & fs, string fn) {
	buffer = 0;
	alGenBuffers (1, &buffer);
	
	const auto vorbis = fs.load_file (fn);
	const auto pcm = VorbisDec::decode_all (vorbis);
	
	pcm.to_openal (buffer);
}

void vorb_to_buffer (ALuint & buffer, const FileSystem & fs, string fn) {
	vorb_to_buffer_tremor (buffer, fs, "sounds/" + fn);
	//vorb_to_buffer_stb (buffer, fs, "sounds/" + fn);
}
*/
void play_sound (ALuint src, ALuint buffer, float gain) {
	ALint current_buffer = buffer;
	alGetSourcei (src, AL_BUFFER, &current_buffer);
	
	//if (current_buffer != buffer) {
		alSourceStop (src);
		alSourcei (src, AL_BUFFER, buffer);
		alSourcef (src, AL_GAIN, gain);
		alSourcePlay (src);
	//}
}
