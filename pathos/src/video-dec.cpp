#include "video-dec.h"

#include <iostream>
#include <string>

using namespace std;

string ffmpeg_error (int errnum) {
	char buffer [256];
	av_make_error_string(buffer, 256, errnum);
	return string (buffer);
}

FfmpegDecoder::FfmpegDecoder (const char * url):
	format_context (nullptr),
	codec_context (nullptr),
	pkt (nullptr),
	frame (nullptr),
	sws_context (nullptr)
{
	av_register_all();
	
	int rc = 0;
	
	rc = avformat_open_input (&format_context, url, nullptr, nullptr);
	
	if (rc != 0) {
		cout << "avformat_open_input " << rc << endl;
	}
	
	AVCodec * video_codec = nullptr;
	
	best_stream = av_find_best_stream (format_context, AVMEDIA_TYPE_VIDEO, -1, -1, &video_codec, 0);
	
	if (video_codec == nullptr) {
		cout << "avcodec_find_decoder" << endl;
	}
	
	codec_context = avcodec_alloc_context3 (video_codec);
	
	if (codec_context == nullptr) {
		cout << "avcodec_alloc_context3" << endl;
	}
	
	rc = avcodec_parameters_to_context(codec_context, format_context->streams [best_stream]->codecpar);
	if (rc < 0) {
		cout << "avcodec_parameters_to_context " << ffmpeg_error(rc) << endl;
	}
	
	rc = avcodec_open2(codec_context, video_codec, nullptr);
	if (rc < 0) {
		cout << "avcodec_open2 " << ffmpeg_error(rc) << endl;
	}
	
	pkt = av_packet_alloc();
	if (pkt == nullptr) {
		cout << "av_packet_alloc" << endl;
	}
	
	frame = av_frame_alloc();
	if (frame == nullptr) {
		cout << "av_frame_alloc" << endl;
	}
}

FfmpegDecoder::~FfmpegDecoder() {
	sws_freeContext(sws_context);
	av_packet_free (&pkt);
	av_frame_free (&frame);
	avcodec_free_context (&codec_context);
	avformat_close_input (&format_context);
}

int FfmpegDecoder::width () const {
	return codec_context->width;
}

int FfmpegDecoder::height () const {
	return codec_context->height;
}

int FfmpegDecoder::time_base_num () const {
	return codec_context->time_base.num;
}

int FfmpegDecoder::time_base_den () const {
	return codec_context->time_base.den;
}

void FfmpegDecoder::init_sws () {
	if (sws_context == nullptr) {
		sws_context = sws_getContext (width (), height (), codec_context->pix_fmt, width (), height (), AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);
		if (sws_context == nullptr) {
			cout << "sws_getContext" << endl;
		}
	}
}

const Image & FfmpegDecoder::next_frame() {
	for (int i = 0; i < 10; i++) {
		int rc = av_read_frame (format_context, pkt);
		if (rc != 0) {
			cout << "av_read_frame" << endl;
		}
		
		if (pkt->stream_index == best_stream) {
			// Decode
			rc = avcodec_send_packet (codec_context, pkt);
			if (rc < 0) {
				cout << "avcodec_send_packet " << ffmpeg_error(rc) << endl;
			}
			
			rc = avcodec_receive_frame (codec_context, frame);
			if (rc == 0) {
				img.w = width ();
				img.h = height ();
				img.channels = 3;
				
				img.pixels.resize (img.w * img.h * img.channels);
				
				int dest_strides [] = {
					width (),
					width (),
					width (),
				};
				
				const auto pixel_count = img.w * img.h;
				uint8_t * const dst[] = {
					&img.pixels [0 * pixel_count],
					&img.pixels [1 * pixel_count],
					&img.pixels [1 * pixel_count + img.w / 2],
				};
				
				init_sws ();
				
				sws_scale (sws_context, frame->data, frame->linesize, 0, height (), dst, dest_strides);
				
				return img;
			}
		}
		else {
			// Audio or subtitles or something boring
		}
		
		av_packet_unref (pkt);
	}
	
	return img;
}
