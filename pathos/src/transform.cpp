#include "transform.h"

#include "glm/gtc/matrix_transform.hpp"
#include <glm/gtc/quaternion.hpp>

using namespace glm;

Transform::Transform () {
	m = mat4 (1.f);
}

Transform::Transform (const glm::mat4 & new_m) : m (new_m) {}

Transform Transform::rotate (float rads, vec3 axis) const {
	return Transform (glm::rotate (mat4 (1.f), rads, axis) * m);
}

Transform Transform::rotate (const quat & q) const {
	return Transform (mat4_cast (q) * m);
}

Transform Transform::scale (vec3 s) const {
	return Transform (glm::scale (mat4 (1.f), s) * m);
}

Transform Transform::scale (float f) const {
	return scale (vec3 (f));
}

Transform Transform::translate (vec3 v) const {
	return Transform (glm::translate (mat4 (1.f), v) * m);
}

Transform Transform::look_at (vec3 eye, vec3 center, vec3 up) const {
	return Transform (glm::lookAt (eye, center, up) * m);
}

Transform Transform::basis (glm::vec3 x, glm::vec3 y, glm::vec3 z) const {
	mat4 rc (1.f);
	rc [0] = vec4 (x, 0.0f);
	rc [1] = vec4 (y, 0.0f);
	rc [2] = vec4 (z, 0.0f);
	
	return Transform (rc * m);
}

Transform Transform::point_at (glm::vec3 forward, glm::vec3 up) const {
	const auto right = cross (forward, up);
	const auto new_forward = cross (up, right);
	
	return basis (right, new_forward, up);
}

Transform Transform::apply (const glm::mat4 & o) const {
	return Transform (o * m);
}

Camera::Camera () {
	yaw = 0;
	pitch = 0;
}

Camera Camera::clamped () const {
	Camera rc;
	rc.yaw = mod (yaw, 360.0f);
	rc.pitch = clamp (pitch, -90.0f, 90.0f);
	return rc;
}
	
