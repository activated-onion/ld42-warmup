#ifndef PATHOS_ECS_H
#define PATHOS_ECS_H

#include <map>
#include <stdint.h>
#include <vector>

typedef uint32_t Entity;

struct ComponentEraser {
	ptrdiff_t offset;
	void (*func_erase) (void * opaque_component, Entity e);
};

template <class T>
using ComponentMap = std::map <Entity, T>;

template <class T>
struct ComponentMap2 {
	explicit ComponentMap2 (std::vector <ComponentEraser> * base_addr) {
		base_addr->push_back (ComponentEraser {
			(uint8_t *)(this) - (uint8_t *)(base_addr),
			static_erase,
		});
	}
	
	ComponentMap <T> & operator * () {
		return map;
	}
	
	const ComponentMap <T> & operator * () const {
		return map;
	}
	
	T & operator [] (Entity e) {
		return map [e];
	}
	
	void erase (Entity e) {
		map.erase (e);
	}
	
	const T & at (Entity e) const {
		return map.at (e);
	}
	
	unsigned int size () const {
		return map.size ();
	}
	
	static void static_erase (void * opaque_that, Entity e) {
		auto that = static_cast <ComponentMap2 <T> *> (opaque_that);
		that->erase (e);
	}
	
	ComponentMap <T> map;
};



struct EntityNamespace {
	EntityNamespace ();
	Entity name_entity ();
private:
	Entity next_entity;
};

// Not sure if this will be useful
struct EntitySelector {
	std::vector <Entity> entities;
	
	EntitySelector () {
		
	}
	
	template <class T>
	EntitySelector (const ComponentMap <T> & init) {
		for (const auto pair : init) {
			entities.push_back (pair.first);
		}
	}
	
	template <class T>
	EntitySelector join (const ComponentMap <T> & m) {
		EntitySelector rc;
		
		for (const auto e : entities) {
			if (m.find (e) != m.end ()) {
				rc.entities.push_back (e);
			}
		}
		
		return rc;
	}
};

#endif
