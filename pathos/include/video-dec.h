#ifndef PATHOS_VIDEO_DEC_H
#define PATHOS_VIDEO_DEC_H

#include <memory>

extern "C" {
	#include "libavutil/avutil.h"
	#include "libavcodec/avcodec.h"
	#include "libavformat/avformat.h"
	#include "libswscale/swscale.h"
}

#include "image.h"

class FfmpegDecoder {
public:
	FfmpegDecoder (const char * url);
	~FfmpegDecoder ();
	FfmpegDecoder (const FfmpegDecoder & other) = delete;
	FfmpegDecoder & operator = (const FfmpegDecoder & other) = delete;
	
	int width () const;
	int height () const;
	int time_base_num () const;
	int time_base_den () const;
	
	const Image & next_frame ();
	
private:
	AVFormatContext * format_context;
	AVCodecContext * codec_context;
	AVPacket * pkt;
	AVFrame * frame;
	SwsContext * sws_context;
	int best_stream;
	
	Image img;
	
	void init_sws ();
};

#endif
