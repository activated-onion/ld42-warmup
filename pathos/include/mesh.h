#ifndef PATHOS_MESH_H
#define PATHOS_MESH_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <stdint.h>
#include <string>
#include <vector>

struct MeshVertex {
	glm::vec4 pos;
	glm::vec2 uv;
	glm::vec3 normal;
};

struct SkinnedMeshVertex {
	glm::vec3 pos;
	glm::vec2 uv;
	glm::vec3 normal;
	uint8_t blend_index [4];
	uint8_t blend_weight [4];
};

// Only used with a MeshBuffer
struct MeshSlice {
	uint16_t start;
	uint16_t size;
	std::string name;
};

// Contains 0+ meshes
struct MeshBuffer {
	std::vector <MeshVertex> verts;
	std::vector <uint16_t> indexes;
	std::vector <MeshSlice> meshes;
	
	static MeshBuffer load_iqm (const std::vector <uint8_t> & bytes);
};

struct Joint {
	std::string name;
	int32_t parent;
	glm::vec3 translate;
	glm::quat rotate;
	glm::vec3 scale;
};

// This is what IQM calls it, even though it makes little sense
struct Pose {
	int32_t parent;
	uint32_t mask;
	float channeloffset [10];
	float channelscale [10];
};

struct Anim {
	std::string name;
	uint32_t first_frame;
	uint32_t num_frames;
	float framerate;
	uint32_t flags;
};

struct SkinnedMeshBuffer {
	std::vector <SkinnedMeshVertex> verts;
	std::vector <uint16_t> indexes;
	std::vector <MeshSlice> meshes;
	
	std::vector <Joint> joints;
	std::vector <Pose> poses;
	uint32_t num_framechannels;
	std::vector <uint16_t> frames;
	
	static SkinnedMeshBuffer load_iqm (const std::vector <uint8_t> & bytes);
};

#endif
