#ifndef PATHOS_RAYCAST_H
#define PATHOS_RAYCAST_H

#include <glm/glm.hpp>
#include <stdint.h>
#include <vector>

struct MeshBuffer;

/* Given a mesh and a dot product threshold for floor triangles,
 * returns a list of indices representing all walkable floor triangles.
 * e.g. if there's 2 walkable triangles, the list will have 6 index elements.
 * If the triangles form a square, it will still be 6 but some of them will
 * be the same.
 */
std::vector <int32_t> find_walkable_tris (const MeshBuffer & mesh, float floor_thresh);

struct Aabb2 {
	glm::vec2 min;
	glm::vec2 max;
};

Aabb2 find_corners (const MeshBuffer & mesh, const std::vector <int32_t> & tris);

struct GridTri {
	int32_t mesh_tri [3];
	int32_t grid_index;
	
	bool operator < (const GridTri & b);
};

struct RaycastGridBin {
	int32_t start;
	int32_t num_tris;
};

struct iAabb2 {
	glm::ivec2 min;
	glm::ivec2 max;
};

struct RaycastGrid {
	/* Given a raycast point such as a player position,
	 * start with 'grid_indices'. This vector contains one
	 * int per grid square. The int is an offset into
	 * the sorted vert array.
	 */
	std::vector <RaycastGridBin> grid_indices;
	
	/* Same as in find_walkable_tris but with some duplicates
	 */
	std::vector <GridTri> tris;
	
	Aabb2 aabb;
	float grid_width;
	int grid_stride;
	
	glm::ivec2 bin_point (glm::vec2 xy) const;
	iAabb2 bin_points (const MeshBuffer & mesh, const int32_t * indices, int32_t num_indices) const;
	int32_t get_bin_index (glm::ivec2) const;
};

RaycastGrid make_grid (
	const MeshBuffer & mesh, 
	const std::vector <int32_t> & walkable_tris,
	float grid_width);

struct Raycast2d {
	bool hit;
	float z;
};

glm::vec3 flatten (glm::vec3 v);

float interpolate_z (glm::vec3 verts [3], glm::vec3 xyz);

Raycast2d raycast (const MeshBuffer & mesh, const std::vector <int32_t> & walkable_tris, glm::vec3 xyz);

Raycast2d raycast_grid (const MeshBuffer & mesh, const RaycastGrid & grid, glm::vec3 xyz);

#endif
