#ifndef PATHOS_INPUT_H
#define PATHOS_INPUT_H

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

enum class EButton {
	Nil,
	Up,
	Left,
	Down,
	Right,
	Pause,
	Click,
	Jump,
	Sink,
	COUNT,
};

struct Buttons {
	bool btns [(int)EButton::COUNT];
	
	Buttons ();
	
	bool get (EButton b) const;
	
	static EButton map (SDL_Scancode key);
};

glm::vec2 get_move_vec (const Buttons & btns_held);
Buttons get_kbd_buttons ();
Buttons get_joy_buttons (SDL_Joystick * joy);
Buttons or_buttons (const Buttons & a, const Buttons & b);

#endif
