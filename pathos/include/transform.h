#ifndef PATHOS_TRANSFORM_H
#define PATHOS_TRANSFORM_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

// This is the class I'm most proud of in all of Pathos
struct Transform {
	glm::mat4 m;
	
	Transform ();
	Transform (const glm::mat4 & new_m);
	
	Transform rotate (float rads, glm::vec3 axis) const;
	Transform rotate (const glm::quat & q) const;
	Transform scale (glm::vec3 s) const;
	Transform scale (float f) const;
	Transform translate (glm::vec3 v) const;
	Transform look_at (glm::vec3 eye, glm::vec3 center, glm::vec3 up) const;
	Transform basis (glm::vec3 x, glm::vec3 y, glm::vec3 z) const;
	// Make sure forward and up are normalized
	Transform point_at (glm::vec3 forward, glm::vec3 up) const;
	Transform apply (const glm::mat4 & o) const;
};

struct Camera {
	// Degrees
	float yaw;
	float pitch;
	
	Camera ();
	Camera clamped () const;
};

#endif
