#ifndef PATHOS_SDL_RAII_H
#define PATHOS_SDL_RAII_H

#include <functional>
#include <memory>

struct SDL_Window;

using up_sdl2 = std::unique_ptr <SDL_Window, std::function <void (SDL_Window *)> >;

struct SdlRaii {
	up_sdl2 sdl;
	
	SdlRaii (int width, int height);
	
	void swap_buffers () const;
};

#endif
