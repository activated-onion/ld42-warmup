#ifndef PATHOS_FILE_SYSTEM_H
#define PATHOS_FILE_SYSTEM_H

#include <stdint.h>
#include <string>
#include <vector>

struct FileSystem {
	FileSystem () {}
	~FileSystem () {}
	
	std::vector <uint8_t> load_file (const std::string & fn) const;
};

#endif
