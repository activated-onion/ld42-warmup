#ifndef PATHOS_TEXT_H
#define PATHOS_TEXT_H

#include <glm/glm.hpp>
#include <stdint.h>
#include <string>
#include <vector>

#include "mesh.h"

struct TextVertex {
	glm::vec4 attr_pos;
	glm::vec2 attr_uv;
};

struct Text {
	std::vector <TextVertex> verts;
	std::vector <uint16_t> indices;
	
	int width;
	int height;
};

std::vector <TextVertex> make_square ();

Text layout_text (const std::string & s);
MeshBuffer layout_text_mesh (const std::string & s);

#endif
