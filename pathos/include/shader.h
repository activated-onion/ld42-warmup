#ifndef PATHOS_SHADER_H
#define PATHOS_SHADER_H

#include <stdint.h>
#include <vector>

#include "pathos-gl.h"

struct ShaderRaii {
	GLuint id;
	
	ShaderRaii (GLenum type);
	~ShaderRaii ();
	
	void source (const std::vector <uint8_t> & source) const;
	
	GLuint operator * () const;
};

struct ShaderProgramRaii {
	GLuint id;
	
	ShaderProgramRaii ();
	~ShaderProgramRaii ();
	
	GLuint operator * () const;
};

#endif
