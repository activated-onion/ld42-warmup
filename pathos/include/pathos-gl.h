#ifndef PATHOS_GL_H
#define PATHOS_GL_H

#ifdef INSID_PANDORA
	// Pandora 
	#include <GLES2/gl2.h>
	#include <eglport.h>
#elif INSID_WIN32
	#include "glew.h"
#else
	#include "GLee.h"
#endif

#endif
