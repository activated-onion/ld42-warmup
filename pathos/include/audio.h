#ifndef PATHOS_AUDIO_H
#define PATHOS_AUDIO_H

#include <stdint.h>
#include <vector>

#include <functional>
#include <memory>
//#include <ivorbisfile.h>
#include "stb-vorbis.h"

#include "AL/al.h"

struct FileSystem;

struct PcmBuffer {
	int nchannels;
	int sample_rate;
	std::vector <int16_t> buffer;
	
	ALenum openal_fmt () const;
	void to_openal (ALuint b) const;
};

extern "C" {
	size_t vorb_read (void * ptr, size_t size, size_t nmemb, void * datasource);
	int vorb_seek (void * datasource, int64_t offset, int whence);
	long vorb_tell (void * datasource);
}
/*
struct VorbisDec {
	const std::vector <uint8_t> buffer;
	size_t cursor;
	std::unique_ptr <OggVorbis_File, std::function <void (OggVorbis_File *)> > ov_handle;
	
	VorbisDec (const std::vector <uint8_t> b);
	
	int read (int16_t * buf, int length);
	
	static PcmBuffer decode_all (const std::vector <uint8_t> & vorbis_data);
};
*/

void vorb_to_buffer_stb (ALuint & buffer, const FileSystem & fs, std::string fn);


//void vorb_to_buffer_tremor (ALuint & buffer, const FileSystem & fs, std::string fn);
//void vorb_to_buffer (ALuint & buffer, const FileSystem & fs, std::string fn);
void play_sound (ALuint src, ALuint buffer, float gain);

#endif
