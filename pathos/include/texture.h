#ifndef PATHOS_TEXTURE_H
#define PATHOS_TEXTURE_H

#include "pathos-gl.h"

// Recommend to only use this with unique_ptr
struct TextureRaii {
	GLuint id;
	
	TextureRaii ();
	~TextureRaii ();
	
	TextureRaii (const TextureRaii & o) = delete;
	TextureRaii & operator = (const TextureRaii & o) = delete;
	
	// Allows use in std::vector's resize ()
	TextureRaii (TextureRaii && o);
	TextureRaii & operator = (TextureRaii && o) = delete;
	
	GLuint operator * () const;
	void bind () const;
};

#endif
