#ifndef PATHOS_IMAGE_H
#define PATHOS_IMAGE_H

#include <stdint.h>
#include <vector>

struct Image {
	int w;
	int h;
	int channels;
	std::vector <uint8_t> pixels;
	
	Image ();
	Image (const std::vector <uint8_t> & compressed);
	~Image ();
};

#endif
