#ifndef PATHOS_TIMESTEP_H
#define PATHOS_TIMESTEP_H

struct Timestep {
	// e.g. num = 60 frames, den = 1000 ms
	Timestep (int num, int den);
	Timestep ();
	
	// e.g. consume (17 ms) -> 1 frame
	int consume (int den);
	
	int accumulator;
	int num, den;
};

#endif
