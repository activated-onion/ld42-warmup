local str = function (name, default)
	return {"std::string", name, default or ""}
end

local int = function (name, default)
	return {"int", name, default or 0}
end

local float = function (name, default)
	return {"float", name, default or 0.0}
end

local map_array = function (array, fn)
	local rc = {}
	
	for i, v in ipairs (array) do
		rc [i] = fn (v)
	end
	
	return rc
end

local function shader_uniforms (vert_fn, frag_fn)
	local fields = {}
	
	local function do_file (fn)
		for line in io.lines (fn) do
			local function do_line (matcher)
				local precision, type, name = line:match (matcher)
				
				if precision and type and name then
					table.insert (fields, int (name))
				end
			end
			
			do_line "^uniform (%S+) (%S+) (%S+) %[%d+%];"
			do_line "^uniform (%S+) (%S+) (%S+);"
			do_line "^attribute (%S+) (%S+) (%S+);"
		end
	end
	
	do_file (vert_fn)
	do_file (frag_fn)
	
	return fields
end

return {
	str = str,
	int = int,
	float = float,
	map_array = map_array,
	shader_uniforms = shader_uniforms,
}
