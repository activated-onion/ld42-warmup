#!/usr/bin/env bash
# Usage:
# some-process | bash write-if-changed.bash out-file

# Make up temp filename
TEMP_NAME="temp-`dd if=/dev/urandom bs=1 count=10 2> /dev/null | base32`.bin"
REAL_NAME=$1

cat > ${TEMP_NAME}

TEMP_HASH="`sha256sum < ${TEMP_NAME}`"

if [ -e "${REAL_NAME}" ];
then
	REAL_HASH="`sha256sum < ${REAL_NAME}`"
else
	REAL_HASH="no."
fi

#echo ${TEMP_HASH}
#echo ${REAL_HASH}

if [ "${TEMP_HASH}" = "${REAL_HASH}" ];
then
	# Files match
	#echo "Match"
	rm ${TEMP_NAME}
else
	# Write
	#echo "Update"
	mv ${TEMP_NAME} ${REAL_NAME}
fi
