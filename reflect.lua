local schema = require (arg [1])

local function print_enum (enum)
	print (("enum %s {"):format (enum.name))
	
	print "}"
end

local function print_struct (struct)
	print (("struct %s {"):format (struct.name))
	
	-- Write fields
	for _, field in ipairs (struct.fields) do
		print (("\t%s %s;"):format (field [1], field [2]))
	end
	
	print "\t"
	
	-- Declare constructors
	print (("\t%s ();"):format (struct.name))
	print (("\t%s (const std::string & name, void * userdata);"):format (struct.name))
	print "};"
	print ()
	
	local initializers = {}
	
	for i, field in ipairs (struct.fields) do
		local value = field [3]
		local value_s = ""
		if value == nil then
			value_s = ""
		elseif type (value) == "boolean" then
			value_s = tostring (value)
		elseif type (value) == "number" then
			value_s = tostring (value)
		elseif type (value) == "string" then
			value_s = ('"%s"'):format (value)
		end
		
		local comma = ','
		if i == #struct.fields then
			comma = ''
		end
		
		table.insert (initializers, ("%s (%s)%s"):format (field [2], value_s, comma))
	end
	
	-- Declare loader
	print (("void load (%s & obj, const std::string & name, void * userdata);"):format (struct.name))
	
	-- Declare visitor
	print (("void visit (%s & obj, Visitor & v);"):format (struct.name))
	
	print ()
	
	-- Begin definitions
	
	print "#ifdef LD41_REFLECTED_IMPL"
	print ()
	
	-- Define loader
	print (("void load (%s & obj, const std::string & name, void * userdata) {"):format (struct.name))
	
	print '\tif (load_push (name, userdata)) {'
	
	for _, field in ipairs (struct.fields) do
		print (('\t\tload (obj.%s, "%s", userdata);'):format (field [2], field [2]))
	end
	
	print '\t}'
	print '\tload_pop (userdata);'
	
	print "}"
	print ()
	
	-- Define constructors
	print (("%s::%s () :"):format (struct.name, struct.name))
	
	for _, init in ipairs (initializers) do
		print ("\t" .. init)
	end
	
	print ("{}")
	print ()
	
	print (("%s::%s (const std::string & name, void * userdata) :"):format (struct.name, struct.name))
	for _, init in ipairs (initializers) do
		print ("\t" .. init)
	end
	print "{ load (*this, name, userdata); }"
	print ()
	
	-- Define visitor
	print (("void visit (%s & obj, Visitor & v) {"):format (struct.name))
		for _, field in ipairs (struct.fields) do
			local visit_func = "visit"
			if field [1] == "int" then
				visit_func = "v.visit_int"
			elseif field [1] == "float" then
				visit_func = "v.visit_float"
			elseif field [1] == "std::string" then
				visit_func = "v.visit_string"
			elseif field [1] == "bool" then
				visit_func = "v.visit_bool"
			end
			
			print (('\t%s ("%s", obj.%s, v);'):format (visit_func, field [2], field [2]))
		end
		
	print "}"
	
	print ()
	
	print "#endif"
	print ()
end

local includes = {
	"string",
	"vector",
}

local local_includes = {
	"reflect-load.h",
}

local guard = "LD41_REFLECTED_" .. arg [1]:gsub ('-', '_')

print ("#ifndef " .. guard)
print ("#define " .. guard)
print "// Generated by reflect.lua"

print ()

for _, include in ipairs (includes) do
	print (("#include <%s>"):format (include))
end

print ()

for _, include in ipairs (local_includes) do
	print (('#include "%s"'):format (include))
end

for _, include in ipairs (schema.includes) do
	print (('#include "%s"'):format (include))
end

print ()

for _, struct in ipairs (schema.defs) do
	if struct.type == 'struct' then
		print_struct (struct)
	elseif struct.type == 'enum' then
		print_enum (struct)
	end
end

print "#endif"
