local wall_radius = 1 / 16
local wall_length = 1 - 2 * wall_radius

local function list_to_set (t)
	local s = {}
	for _, v in ipairs (t) do
		s [v] = true
	end
	return s
end

local has_east = list_to_set {
	2,
	18,
	20,
}

local has_south = list_to_set {
	3,
	17,
	18,
}

local has_east_door = list_to_set {
	3,
	4
}

local has_south_door = list_to_set {
	19,
	20,
}

local last_vert = 0
local verts = {}
local texcoords = {}
local normals = {}
local faces = {}

local function add_face (face_verts)
	local face = {}
	for _, vert in ipairs (face_verts) do
		table.insert (verts, vert)
		last_vert = last_vert + 1
		table.insert (face, last_vert)
	end
	
	table.insert (faces, face)
end

local function add_uv_rect (t)
	table.insert (texcoords, {t.left,  t.bottom})
	table.insert (texcoords, {t.right, t.bottom})
	table.insert (texcoords, {t.right, t.top})
	table.insert (texcoords, {t.left,  t.top})
end

local function add_normal (n)
	table.insert (normals, n)
end

local wall_uv = {
	left = 0,
	top = 0,
	right = 16,
	bottom = 48,
}

local corner_uv = {
	left = 128 - 16,
	top = 128 - 48,
	right = 128 - 16 + 2,
	bottom = 128,
}

local blank_uv = {
	left = 128,
	top = 128,
	right = 128,
	bottom = 128,
}

local ceiling_uv = {
	left = 16,
	top = 64,
	right = 32,
	bottom = 80,
}

local function sum_bools (t)
	local sum = 0
	for _, b in ipairs (t) do
		if b then
			sum = sum + 1
		end
	end
	return sum
end

local function map (t, fn)
	local rc = {}
	for i, v in ipairs (t) do
		rc [i] = fn (v)
	end
	return rc
end

local function bool (b)
	return not not b
end

local function add2 (a, b)
	return {
		a [1] + b [1],
		a [2] + b [2],
	}
end

local function neg2 (a)
	return {
		-a [1],
		-a [2],
	}
end

local function sub2 (a, b)
	return {
		a [1] - b [1],
		a [2] - b [2],
	}
end

local function perpendicular2 (a)
	return {
		-a [2],
		a [1],
	}
end

local function mul2 (a, s)
	return {
		a [1] * s,
		a [2] * s,
	}
end

-- 2D, not squared!
local function length2 (a)
	return math.sqrt (a [1] * a [1] + a [2] * a [2])
end

local function normalize2 (a)
	return mul2 (a, 1 / length2 (a))
end

local function vec2_to_3 (v, z)
	return {v [1], v [2], z}
end

local map_t = require 'map'

for story = 0, 2 do
	local layer = map_t.layers [story * 3 + 2]
	local data = layer.data
	local floor_data = map_t.layers [story * 3 + 1].data
	local color_data = map_t.layers [story * 3 + 3].data

	local center_x = layer.width / 2
	local center_y = layer.height / 2

	local function build_tile (x, y)
		local function get_index (v)
			return (y + v [2]) * layer.width + x + v [1] + 1
		end
		local function get_tile (v)
			return data [get_index (v)]
		end
		
		-- What tile owns this wall direction?
		local function wall_to_tile (w)
			local x = math.min (w [1], 0)
			local y = math.min (w [2], 0)
			
			return {x, y}
		end
		
		-- Go to the center of tile v
		-- and look in direction w
		-- Is there a wall there?
		local function get_wall (v, w) 
			local tile = get_tile (add2 (v, wall_to_tile (w)))
			if w [1] == 0 then
				return bool (has_south [tile])
			elseif w [2] == 0 then
				return bool (has_east [tile])
			end
		end
		
		local tile = data [y * layer.width + x + 1]
		local tile_e = data [y * layer.width + x + 1 + 1]
		local tile_s = data [(y + 1) * layer.width + x + 1]
		local tile_w = data [y * layer.width + x - 1 + 1]
		local tile_n = data [(y - 1) * layer.width + x + 1]
		
		if true or tile > 0 then
			--local rx, ry = x - center_x, y - center_y
			local rx, ry = x, y
			local r = wall_radius
			
			local function add_rel_face (face_verts)
				add_face (map (face_verts, function (vert) 
					return {rx + vert [1], ry + vert [2], vert [3] + (3 + 0.125) * story}
				end))
			end
			
			local wall_height = 3
			local door_height = 2.25
			local function add_rel_wall (v)
				add_rel_face {
					{v [1][1], v [1][2], v [1][3]},
					{v [2][1], v [2][2], v [2][3]},
					{v [2][1], v [2][2], wall_height},
					{v [1][1], v [1][2], wall_height},
				}
				
				local n = normalize2 (perpendicular2 (sub2 (v [2], v [1])))
				
				add_normal {n [1], n [2], 0}
			end
			
			local floor_tile = floor_data [get_index {0, 0}] - 256
			
			local function f (b)
				if b then
					return -r
				else
					return 0
				end
			end
			
			local left  = floor_data [get_index {-1,  0}] == 0
			local right = floor_data [get_index { 1,  0}] == 0
			local up    = floor_data [get_index { 0, -1}] == 0
			local down  = floor_data [get_index { 0,  1}] == 0
			
			if floor_tile >= 1 then
				floor_u = (floor_tile - 1) % 8
				floor_v = (floor_tile - 1 - floor_u) / 8
				
				
				local v = {
					{1 - f (right), 1 - f (down), 0},
					{1 - f (right), f (up), 0},
					{f (left), f (up), 0},
					{f (left), 1 - f (down), 0},
				}
				
				add_rel_face (v)
				
				local floor_uv = {
					left = floor_u * 16,
					top = floor_v * 16,
					right = floor_u * 16 + 16,
					bottom = floor_v * 16 + 16,
				}
				add_uv_rect (floor_uv)
				add_normal {0, 0, 1}
			end
			
			if floor_data [get_index {0, 0}] > 0 then
				local wall_height = -0.125
				local next_floor = wall_height + 0.125
				
				local v = {
					{f (left), f (up)},
					{1 - f (right), f (up)},
					{1 - f (right), 1 - f (down)},
					{f (left), 1 - f (down)},
				}
				
				local function add_rel_wall (v, lower, upper)
					add_rel_face {
						{v [1][1], v [1][2], lower},
						{v [2][1], v [2][2], lower},
						{v [2][1], v [2][2], upper},
						{v [1][1], v [1][2], upper},
					}
					
					local n = normalize2 (perpendicular2 (sub2 (v [2], v [1])))
					
					add_uv_rect (corner_uv)
					add_normal {n [1], n [2], 0}
				end
				
				if left then
					add_rel_wall ({v [1], v [4]}, wall_height, next_floor)
				end
				if up then
					add_rel_wall ({v [2], v [1]}, wall_height, next_floor)
				end
				if right then
					add_rel_wall ({v [3], v [2]}, wall_height, next_floor)
				end
				if down then
					add_rel_wall ({v [4], v [3]}, wall_height, next_floor)
				end
				
				do
					add_rel_face {
						{f (left), f (up), wall_height},
						{1 - f (right), f (up), wall_height},
						{1 - f (right), 1 - f (down), wall_height},
						{f (left), 1 - f (down), wall_height},
					}
					add_uv_rect (ceiling_uv)
					add_normal {0, 0, -1}
				end
			end
			
			if has_east [tile] then
				add_rel_face {
					{1 - r, r,     wall_height},
					{1 - r, 1 - r, wall_height},
					{1 + r, 1 - r, wall_height},
					{1 + r, r,     wall_height},
				}
				add_uv_rect (corner_uv)
				add_normal {0, 0, 1}
			end
			if has_south [tile] then
				add_rel_face {
					{1 - r, 1 - r, wall_height},
					{    r, 1 - r, wall_height},
					{    r, 1 + r, wall_height},
					{1 - r, 1 + r, wall_height},
				}
				add_uv_rect (corner_uv)
				add_normal {0, 0, 1}
			end
			
			local dirs = {
				{ 1,  0},
				{ 0,  1},
				{-1,  0},
				{ 0, -1},
			}
			
			for _, dir in ipairs (dirs) do
				if get_wall ({0, 0}, dir) then
					do
						local right = neg2 (perpendicular2 (dir))
						local left = neg2 (right)
						local down = perpendicular2 (right)
						local up = neg2 (down)
						
						local start = 1 - r
						local stop = r
						if not get_wall ({0, 0}, right) and get_wall (right, down) then
							start = 1
						end
						if not get_wall ({0, 0}, left) and get_wall (left, down) then
							stop = 0
						end
						
						local offset = add2 ({1, 1}, add2 (wall_to_tile (left), wall_to_tile (up)))
						
						add_rel_wall {
							vec2_to_3 (add2 (offset, add2 (mul2 (right, start), mul2 (down, 1 - r))), 0),
							vec2_to_3 (add2 (offset, add2 (mul2 (right, stop), mul2 (down, 1 - r))), 0),
						}
						
						local color = math.max (33, color_data [get_index {0, 0}]) - 33
						
						add_uv_rect {
							left = wall_uv.left + (wall_uv.right - wall_uv.left) * (1 - start) + color * 32,
							top = wall_uv.top,
							right = wall_uv.left + (wall_uv.right - wall_uv.left) * (1 - stop) + color * 32,
							bottom = wall_uv.bottom,
						}
					end
				end
			end
			
			if has_south_door [tile] then
				add_rel_wall {
					{1 - r, 1 - r, door_height},
					{r,     1 - r, door_height},
				}
				add_uv_rect (blank_uv)
				add_rel_wall {
					{r,     1 + r, door_height},
					{1 - r, 1 + r, door_height},
				}
				add_uv_rect (blank_uv)
				
				add_rel_face {
					{1 - r, 1 - r, wall_height},
					{    r, 1 - r, wall_height},
					{    r, 1 + r, wall_height},
					{1 - r, 1 + r, wall_height},
				}
				add_uv_rect (corner_uv)
				add_normal {0, 0, 1}
				
				add_rel_face {
					{1 - r, 1 - r, door_height},
					{1 - r, 1 + r, door_height},
					{    r, 1 + r, door_height},
					{    r, 1 - r, door_height},
				}
				add_uv_rect (blank_uv)
				add_normal {0, 0, -1}
			end
			if has_east_door [tile] then
				add_rel_wall {
					{1 - r, r, door_height},
					{1 - r, 1 - r, door_height},
				}
				add_uv_rect (blank_uv)
				add_rel_wall {
					{1 + r, 1 - r, door_height},
					{1 + r,     r, door_height},
					
				}
				add_uv_rect (blank_uv)
				
				add_rel_face {
					{1 + r,     r, wall_height},
					{1 - r,     r, wall_height},
					{1 - r, 1 - r, wall_height},
					{1 + r, 1 - r, wall_height},
				}
				add_uv_rect (corner_uv)
				add_normal {0, 0, 1}
				
				add_rel_face {
					{1 + r,     r, door_height},
					{1 + r, 1 - r, door_height},
					{1 - r, 1 - r, door_height},
					{1 - r,     r, door_height},
				}
				add_uv_rect (blank_uv)
				add_normal {0, 0, -1}
			end
			
			local walls = {
				bool (has_east [tile]),
				bool (has_south [tile]),
				bool (has_east [tile_s]),
				bool (has_south [tile_e]),
			}
			local wall_count = sum_bools (walls)
			
			-- Add corner bits
			if wall_count >= 1 then
				if not walls [1] then
					if walls [2] and walls [4] then
						--add_uv_rect (wall_uv)
					else
						add_rel_wall {
							{1 + r, 1 - r, 0},
							{1 - r, 1 - r, 0},
						}
						add_uv_rect (corner_uv)
					end
				end
				if not walls [2] then
					if walls [1] and walls [3] then
						--add_uv_rect (wall_uv)
					else
						add_rel_wall {
							{1 - r, 1 - r, 0},
							{1 - r, 1 + r, 0},
						}
						add_uv_rect (corner_uv)
					end
				end
				if not walls [3] then
					if walls [2] and walls [4] then
						--add_uv_rect (wall_uv)
					else
						add_rel_wall {
							{1 - r, 1 + r, 0},
							{1 + r, 1 + r, 0},
						}
						add_uv_rect (corner_uv)
					end
				end
				if not walls [4] then
					if walls [1] and walls [3] then
						--add_uv_rect (wall_uv)
					else
						add_rel_wall {
							{1 + r, 1 + r, 0},
							{1 + r, 1 - r, 0},
						}
						add_uv_rect (corner_uv)
					end
				end
				
				add_rel_face {
					{1 + r, 1 - r, wall_height},
					{1 - r, 1 - r, wall_height},
					{1 - r, 1 + r, wall_height},
					{1 + r, 1 + r, wall_height},
				}
				add_uv_rect (corner_uv)
				add_normal {0, 0, 1}
			end
		end
	end

	-- I cut off a row and a column at the end to make the inner loop
	-- for corners simpler
	for y = 1, layer.height - 2 do
		for x = 1, layer.width - 2 do
			build_tile (x, y)
		end
	end
end

for _, vert in ipairs (verts) do
	-- Flip the Y coord right before export, cause
	-- TILED uses raster order but Blender uses right-handed coords
	print (string.format ("v %f %f %f", vert [1], vert [3], vert [2]))
end

print ()

local uv_map_size = 128
for _, uv in ipairs (texcoords) do
	-- Mess up the V coord cause GIMP uses raster order and GL doesn't
	-- or something
	print (string.format ("vt %f %f", uv [1] / uv_map_size, 1.0 - (uv [2] / uv_map_size)))
end
--[[
print "vt 0.0   0.75"
print "vt 0.125 0.75"
print "vt 0.125 1.0"
print "vt 0.0   1.0"
--]]
print ()

for _, n in ipairs (normals) do
	print (string.format ("vn %f %f %f", n [1], n [3], n [2]))
end

print ()
print "s off"
print ()

for i, face in ipairs (faces) do
	print (string.format ("f %i/%i/%i %i/%i/%i %i/%i/%i %i/%i/%i", 
		face [3], face [3], i,
		face [2], face [2], i,
		face [1], face [1], i,
		face [4], face [4], i
	))
end
