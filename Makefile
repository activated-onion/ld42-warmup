all: build/OfficeDemo

build/OfficeDemo: include/main.gen.h include/shader-vars.gen.h CMakeLists.txt meshes/office.iqm building.bin
	(cd build && make)

.PHONY: build/OfficeDemo

include/shader-vars.gen.h: reflect.lua shader-vars-schema.lua reflect-schema-common.lua reflect-shaders.lua shaders/pass.vert shaders/pass.frag shaders/uv.vert shaders/uv.frag shaders/skinned.vert shaders/skinned.frag shaders/text.vert shaders/text.frag shaders/envmap.vert shaders/envmap.frag shaders/decal.vert shaders/decal.frag
	lua $< shader-vars-schema | bash write-if-changed.bash $@

include/main.gen.h: reflect.lua reflect-schema.lua reflect-schema-common.lua reflect-shaders.lua
	lua $< reflect-schema | bash write-if-changed.bash $@

meshes/office.iqm: building.obj
	../iqm/iqm $@ $<

building.obj: map.lua gen-building.lua
	luajit gen-building.lua > $@

building.bin: map.lua gen-building-bin.lua
	luajit gen-building-bin.lua

map.lua: map.tmx
	tiled --export-map $< $@

# textures-premul/tiles.png: textures/tiles.png
#	convert $< -background black -alpha Remove $< -compose Copy_Opacity -composite $@
